MAXEL Killian TP1A
RANIVO Michael TP2A
DEMOUGIN Alexandre TP1A

--------------------

PRE-REQUIS

vcpkg
cmake
box2d
gf
sfml

--------------------
SUR WINDOWS

git clone lien
créer dossier build
se rendre dans dossier build (cd build)

executer la commande ci dessous

cmake ../ -DCMAKE_TOOLCHAIN_FILE=dossier_contenant_vcpkg/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_INSTALL_PREFIX=dossier_contenant_vcpkg/vcpkg/vcpkg/installed/x64-windows -DGF_BUILD_GAMES=ON -DGF_BUILD_TOOLS=ON

ouvrir le fichier .sln
choisir release x64 sur visual studio

clic droit sur all_build : générer solution/build solution/build
clic droit sur install : générer solution/build solution/build

le programme .exe se trouve dans le dossier release

--------------

SUR UBUNTU

git clone lien
mkdir build
cd build
cmake ..
make
make install
./Plateformer2021