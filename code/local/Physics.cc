#include <set>
#include <vector>

#include <gf/Array2D.h>
#include <gf/Color.h>
#include <gf/Curves.h>
#include <gf/Log.h>
#include <gf/Polyline.h>
#include <gf/RenderTarget.h>
#include <gf/Shapes.h>
#include <gf/Unused.h>
#include <gf/VectorOps.h>

#include "Physics.h"

namespace plateformer2021 {

    bool isOnAir = false;

        b2Vec2 lastGroundPosition = {0,0};//dernier sol sur lequel a march� le joueur

        int numFootContacts = 0;//pour emp�cher le saut sans �tre sur surface physique ou sur surface particuli�re
        MyContactListener contactListener;//en variable globale si dans le constructeur de physics, core dumped

        void MyContactListener::BeginContact(b2Contact* contact) {
            //check if fixture A was the foot sensor
            b2FixtureUserData fixtureUserData = contact->GetFixtureA()->GetUserData();
            if (fixtureUserData.pointer == 3) {
                //A est footSensor alors B est soit un bloc, soit une �chelle

                b2FixtureUserData fixtureUserData2 = contact->GetFixtureB()->GetUserData();
                
                if (fixtureUserData2.pointer != 5) {
                    numFootContacts++;
                    //printf("numfootContact begin a : %i\n", numFootContacts);
                    //printf("Begin CONTACT A\n");
                    //isOnAir = false;
                    lastGroundPosition = contact->GetFixtureA()->GetBody()->GetPosition();
                }
            }

            //check if fixture B was the foot sensor
            fixtureUserData = contact->GetFixtureB()->GetUserData();
            if (fixtureUserData.pointer == 3) {
                //B est footSensor alors A est soit un bloc, soit une �chelle
                b2FixtureUserData fixtureUserData2 = contact->GetFixtureA()->GetUserData();
                if (fixtureUserData2.pointer != 5) {
                    numFootContacts++;
                    //printf("numfootContact begin b : %i\n", numFootContacts);
                    //printf("BEGIN CONTACT B\n");
                    isOnAir = false;
                    lastGroundPosition = contact->GetFixtureB()->GetBody()->GetPosition();
                }
            }
        }

        void MyContactListener::EndContact(b2Contact* contact) {
            //check if fixture A was the foot sensor
            b2FixtureUserData fixtureUserData = contact->GetFixtureA()->GetUserData();
            if (fixtureUserData.pointer == 3) {
                b2FixtureUserData fixtureUserData2 = contact->GetFixtureB()->GetUserData();
                if (fixtureUserData2.pointer != 5) {
                    numFootContacts--;
                    //printf("numfootContact end a : %i\n", numFootContacts);
                    lastGroundPosition = contact->GetFixtureA()->GetBody()->GetPosition();
                    //printf("END CONTACT A\n");
                    isOnAir = true;
                }
            }
            //check if fixture B was the foot sensor
            fixtureUserData = contact->GetFixtureB()->GetUserData();
            if (fixtureUserData.pointer == 3) {
                b2FixtureUserData fixtureUserData2 = contact->GetFixtureA()->GetUserData();
                if (fixtureUserData2.pointer != 5) {
                    numFootContacts--;
                    //printf("numfootContact end b : %i\n", numFootContacts);
                    lastGroundPosition = contact->GetFixtureB()->GetBody()->GetPosition();
                    //printf("END CONTACT B\n");
                    isOnAir = true;
                }
            }
        }

	constexpr float PhysicsScale = 0.01f;
    constexpr float Scale = 100.0f;

    int countJump = 0;

    b2Vec2 fromVec(gf::Vector2f vec) {
        return { vec.x * PhysicsScale, vec.y * PhysicsScale };
    }

    gf::Vector2f toVec(b2Vec2 vec) {
        return { vec.x / PhysicsScale, vec.y / PhysicsScale };
    }

    b2World world2({ 0.0f, 60.0f });

    Physics::Physics(Player& hero, MapScene& mp2)
        : world(&world2)
        , body(nullptr)
        , player(hero)
        , gravity({ 0.0f,60.0f })
        , mp(mp2)
    {
        jumpTimeout = 0;
    }

    void Physics::createPhysicalPlayer() {
        gf::Vector2f initialPosition = player.getDynamics().position;
        //printf("x : %p", player.getDynamics().position.x);

        //CORPS PHYSIQUE DU JOUEUR
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = fromVec(initialPosition);
        body = world->CreateBody(&bodyDef);
        body->SetFixedRotation(true);//pour ne pas avoir l'effet d'un d� jet� qui sur une extremit� chute/glisse
        b2PolygonShape ShapeK;
        ShapeK.SetAsBox((80.f / 2) * PhysicsScale / 8, (110.f / 2) * PhysicsScale / 8);
        b2FixtureDef fixtureDef;
        fixtureDef.density = 1.f;
        fixtureDef.friction = 0.5f;//pour glisser et se laisser emporter par une plateforme
        fixtureDef.restitution = 0.0f;//renvoi apr�s choc
        fixtureDef.shape = &ShapeK;
        body->CreateFixture(&fixtureDef);

        //on ajoute un capteur pour les pieds
        ShapeK.SetAsBox((80.f / 2) * PhysicsScale / 16, 0.05, b2Vec2(0, (110.f / 2) * PhysicsScale / 8), 0);
        fixtureDef.isSensor = true;
        b2Fixture* footSensorFixture;
        footSensorFixture = body->CreateFixture(&fixtureDef);
        footSensorFixture->GetUserData().pointer = 3;
        footSensorFixture->SetSensor(true);

        world->SetContactListener(&contactListener);
    }

    void Physics::generateTutorialPhysicalWorld() {

        climbedBodies.clear();
        movingBodies.clear();
        elevatorBodies.clear();
        fallingBodies.clear();
        pushableBodies.clear();
        staticBodies.clear();
        trapBodies.clear();
        ennemiesBodies.clear();

        pushableBodiesInitialPos.clear();
        fallingBodiesInitialPos.clear();
        ennemiesBodiesInitialPos.clear();
        movingBodiesInitialPos.clear();
        elevatorBodiesInitialPos.clear();
        wallBodies.clear();

        mp.climbedBodiesFromPhysics.clear();
        mp.fallingBodiesFromPhysics.clear();
        mp.movingBodiesFromPhysics.clear();
        mp.elevatorBodiesFromPhysics.clear();
        mp.pushableBodiesFromPhysics.clear();
        mp.staticBodiesFromPhysics.clear();
        mp.trapBodiesFromPhysics.clear();
        mp.wallBodiesFromPhysics.clear();

        mp.cling.clear();
        mp.fallingBodiesInitialPosFromPhysics.clear();

        printf("\nOLD SIZE of static bodies : %zu\n", staticBodies.size());

        player.initialPosition = { 50, 200 };
        player.position= { 50, 200 };

        createStaticGround(0.0f, 222.2f);
        createStaticGround(50.0f / 2, 222.2f);
        createStaticGround(100.0f / 2, 222.2f);
        createStaticGround(150.0f / 2, 222.2f);
        createStaticGround(200.0f / 2, 222.2f);
        createStaticGround(250.0f / 2, 222.2f);
        createStaticGround(300.0f / 2, 222.2f);
        createStaticGround(350.0f / 2, 222.2f);
        createStaticGround(400.0f / 2, 222.2f);
        createStaticGround(450.0f / 2, 222.2f);

        createEnnemy(300.0f / 2, 222.2f);

        createFallingGround(450.0f / 2, 160.0f);
        createFallingGround(500.0f / 2, 160.0f);
        createFallingGround(580.0f / 2, 160.0f);

        createMovingGround(500.0f / 2, 222.2f);//bloc qui bouge de gauche � droite

        createElevatorGround(700.0f / 2, 222.2f);//bloc qui bouge de bas en haut

        //obstacle non poussable

        createStaticGround(900.0f / 2, 197.2f);

        //sol plus loin
        createStaticGround(800.0f / 2, 222.2f);
        createStaticGround(850.0f / 2, 222.2f);
        createStaticGround(900.0f / 2, 222.2f);
        createStaticGround(950.0f / 2, 222.2f);

        //obstacle poussable
        createPushableObject(250.0f / 2, 197.2f);

        //echelle
        createClimbedBlock(0.0f / 2, 197.2f);
        createClimbedBlock(0.0f / 2, 172.2f);
        createClimbedBlock(0.0f / 2, 147.2f);

        createClimbedBlock(130.0f / 2, 197.2f);
        createClimbedBlock(130.0f / 2, 172.2f);
        createClimbedBlock(130.0f / 2, 147.2f);

        createClimbedBlock(700.0f / 2, 160.0f);

        createFallingGround(75.0f / 2, 147.2f);

        createTrapBlock(500.0f/2, 120.0f);

        //murs invisibles
        createInvisibleWallLeft();
        createInvisibleWallRight();
        createInvisibleWallTop();

        mp.endFlagPos = { 4.97f,2.36f };
        player.endFlagPos = { 4.97f,2.36f };

        player.setPlayerBodyFromPhysics(body);

        printf("NEW SIZE of static bodies : %zu\n", staticBodies.size());

        player.lowestground = 400.0f;
    }

    void Physics::generatePhysicalWorld() {

        climbedBodies.clear();
        elevatorBodies.clear();
        movingBodies.clear();
        fallingBodies.clear();
        pushableBodies.clear();
        staticBodies.clear();
        trapBodies.clear();
        ennemiesBodies.clear();

        pushableBodiesInitialPos.clear();
        fallingBodiesInitialPos.clear();
        ennemiesBodiesInitialPos.clear();
        movingBodiesInitialPos.clear();
        elevatorBodiesInitialPos.clear();
        wallBodies.clear();

        mp.climbedBodiesFromPhysics.clear();
        mp.fallingBodiesFromPhysics.clear();

        mp.cling.clear();
        mp.fallingBodiesInitialPosFromPhysics.clear();

        mp.movingBodiesFromPhysics.clear();
        mp.elevatorBodiesFromPhysics.clear();
        mp.pushableBodiesFromPhysics.clear();
        mp.staticBodiesFromPhysics.clear();
        mp.trapBodiesFromPhysics.clear();
        mp.ennemiesBodiesFromPhysics.clear();
        mp.wallBodiesFromPhysics.clear();

        plateformer2021::Procedural mapGenerator;

        int tailleBloc = 25;

        printf("SIZE OF PATH -> %zu\n", mapGenerator.map.size());

        /*size_t indice = mapGenerator.start_room;
        size_t x = indice % mapGenerator.largeurMatrice;
        size_t y = (indice - x) / mapGenerator.largeurMatrice;*/

        // Placement du joueur 
        /*float positionX_player = 10.0f;//(x*mapGenerator.largeurMatrice) + ((mapGenerator.map.begin() + mapGenerator.start_room)->checkpointX)*tailleBloc;
        float positionY_player = 20.0f;//(y*mapGenerator.hauteurMatrice) + ((mapGenerator.map.begin() + mapGenerator.start_room)->checkpointY) * tailleBloc;

        player.position = { positionX_player,positionY_player };
        player.initialPosition = { positionX_player,positionY_player };*/

        // Placement des blocs de la map 
        for (int hautMat = 0; hautMat < mapGenerator.hauteurMatrice; ++hautMat) {

            for (int hautSouMat = 0; hautSouMat < mapGenerator.hauteurSousMatrice; ++hautSouMat) {

                for (int largMat = 0; largMat < mapGenerator.largeurMatrice; ++largMat) {

                    for (int largSouMat = 0; largSouMat < mapGenerator.largeurSousMatrice; ++largSouMat) {

                        char bloc = ((mapGenerator.map.begin() + (hautMat * mapGenerator.largeurMatrice) + largMat)->matrice[(hautSouMat * mapGenerator.largeurSousMatrice) + largSouMat]);
                        float posX = ((mapGenerator.largeurSousMatrice * largMat) + largSouMat) * tailleBloc;
                        float posY = ((mapGenerator.hauteurSousMatrice * hautMat) + hautSouMat) * tailleBloc;
                        //createStaticGround(posX, posY);

                        switch (bloc) {

                        case 'P':
                            //player.position = { posX+2.5f,posY+3.5f };
                            player.position = { posX,posY };
                            player.initialPosition = { posX,posY };
                            printf("(%f,%f) (%f,%f) (%f,%f)\n", player.position.x, player.position.y, player.initialPosition.x, player.initialPosition.y, posX, posY);
                            break;

                            // Ascenseur
                        case 'A':
                            createElevatorGround(posX, posY);
                            break;
                            // Scie 
                        case 'C':
                            createTrapBlock(posX, posY);
                            break;
                            // Ennemis mobile 
                        case 'E':
                            createEnnemy(posX, posY + 25);
                            break;
                            // Echelle
                        case 'H':
                            createClimbedBlock(posX, posY);
                            break;
                            // Plateforme mobile 
                        case 'M':
                            createMovingGround(posX, posY);
                            break;
                            // Sol 
                        case 'S':
                            createStaticGround(posX, posY);
                            break;
                            //Plateforme tombante
                        case 'T':
                            createFallingGround(posX, posY);
                            break;
                            // Mur 
                        case 'W':
                            createStaticWall(posX, posY);
                            break;
                            //drapeau de fin
                        case 'F':
                            mp.endFlagPos = { (posX + 15.0f) * PhysicsScale,(posY + 40.0f) * PhysicsScale };
                            player.endFlagPos = { (posX + 15.0f) * PhysicsScale,(posY + 40.0f) * PhysicsScale };
                            break;
                        }
                    }
                }
            }
        }

        player.setPlayerBodyFromPhysics(body);

        player.lowestground = 50.0f;
        for (size_t i = 0; i < staticBodies.size(); i++) {
            if (staticBodies.at(i)->GetPosition().y*100 >= player.lowestground) {
                player.lowestground = staticBodies.at(i)->GetPosition().y*100;
            }
        }
        player.lowestground += 100.0f;
    }

    void Physics::createInvisibleWallLeft() {
        b2BodyDef BodyDef;
        gf::Vector2f position = { -5,281 };//background a hauteur de 562
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_staticBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox(PhysicsScale, (562.f / 2) * PhysicsScale);
        //Shape.SetAsBox((50.0f / 2) / 50.f, (50.f / 2) / 30.f);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 1.0f;
        FixtureDef.friction = 0.0f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);

    }

    void Physics::createInvisibleWallRight() {
        b2BodyDef BodyDef;
        gf::Vector2f position = { 500,281 };//background a hauteur de 562
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_staticBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox(PhysicsScale, (562.f / 2) * PhysicsScale);
        //Shape.SetAsBox((50.0f / 2) / 50.f, (50.f / 2) / 30.f);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 1.0f;
        FixtureDef.friction = 0.0f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
    }

    void Physics::createInvisibleWallTop() {
        b2BodyDef BodyDef;
        gf::Vector2f position = { 10,55 };//background a hauteur de 562
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_staticBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox((1000 / 2)* PhysicsScale, PhysicsScale);
        //Shape.SetAsBox((50.0f / 2) / 50.f, (50.f / 2) / 30.f);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 1.0f;
        FixtureDef.friction = 0.0f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
    }

    void Physics::createStaticGround(float X, float Y) {

        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_staticBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 1.0f;
        FixtureDef.friction = 0.8f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
            staticBodies.push_back(Body);
            mp.staticBodiesFromPhysics.push_back(Body);
    }

    void Physics::createStaticWall(float X, float Y) {

        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_staticBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 1.0f;
        FixtureDef.friction = 0.8f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
        wallBodies.push_back(Body);
        mp.wallBodiesFromPhysics.push_back(Body);
    }

    void Physics::createPushableObject(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_dynamicBody;
        b2Body* Body = world->CreateBody(&BodyDef);

        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 0.2f;
        FixtureDef.friction = 1.0f;
        FixtureDef.restitution = 0.0f;//effet similaire � une balle qui rebondit sur le sol
        FixtureDef.shape = &Shape;
        Body->SetFixedRotation(true);
        Body->CreateFixture(&FixtureDef);
            pushableBodies.push_back(Body);
            pushableBodiesInitialPos.push_back(Body->GetPosition());
            mp.pushableBodiesFromPhysics.push_back(Body);
    }

    void Physics::createMovingGround(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_kinematicBody;
        b2Body* Body = world->CreateBody(&BodyDef);
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 50.0f;
        FixtureDef.friction = 0.8f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
        Body->SetFixedRotation(true);
        movingBodies.push_back(Body);
        movingBodiesInitialPos.push_back(Body->GetPosition());
        mp.movingBodiesFromPhysics.push_back(Body);
    }

    void Physics::createElevatorGround(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_kinematicBody;
        b2Body* Body = world->CreateBody(&BodyDef);
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 50.0f;
        FixtureDef.friction = 0.8f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
        Body->SetFixedRotation(true);
        elevatorBodies.push_back(Body);
        elevatorBodiesInitialPos.push_back(Body->GetPosition());
        mp.elevatorBodiesFromPhysics.push_back(Body);
    }

    void Physics::createFallingGround(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_kinematicBody;
        b2Body* Body = world->CreateBody(&BodyDef);
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 0.0f;//quand le joueur sera dessus, la densit� changera de valeur
        FixtureDef.friction = 0.0f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
        Body->SetFixedRotation(true);
        Body->GetUserData().pointer = 10;
        fallingBodies.push_back(Body);
        fallingBodiesInitialPos.push_back(Body->GetPosition());
        mp.cling.push_back(false);
        mp.fallingBodiesFromPhysics.push_back(Body);
        mp.fallingBodiesInitialPosFromPhysics.push_back(Body->GetPosition());
    }

    void Physics::createClimbedBlock(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        //pas de type de bloc
        b2FixtureDef FixtureDef;
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        FixtureDef.shape = &Shape;
        FixtureDef.isSensor = true;

        b2Body* Body = world->CreateBody(&BodyDef);
        Body->SetFixedRotation(true);
        b2Fixture* ladderSensorFixture;
        ladderSensorFixture=Body->CreateFixture(&FixtureDef);
        ladderSensorFixture->GetUserData().pointer = 5;//pour emp�cher des sauts exponentiels au contact d'une �chelle
        ladderSensorFixture->SetSensor(true);
        climbedBodies.push_back(Body);
        mp.climbedBodiesFromPhysics.push_back(Body);
    }

    void Physics::createTrapBlock(float X,float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 40.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_dynamicBody;
        b2Body* Body = world->CreateBody(&BodyDef);
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 2, (50.f / 2) * PhysicsScale / 2);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 0.0f;
        FixtureDef.friction = 0.0f;
        FixtureDef.restitution = 0.0f;
        FixtureDef.shape = &Shape;
        FixtureDef.isSensor = true;
        Body->CreateFixture(&FixtureDef);
        Body->SetFixedRotation(true);
        Body->SetAngularVelocity(2); //pour tourner
        trapBodies.push_back(Body);
        mp.trapBodiesFromPhysics.push_back(Body);
    }

    void Physics::createEnnemy(float X, float Y) {
        b2BodyDef BodyDef;
        gf::Vector2f position = { X + 12.5f,Y + 20.0f };
        BodyDef.position = fromVec(position);
        BodyDef.type = b2_kinematicBody;
        b2Body* Body = world->CreateBody(&BodyDef);
        b2PolygonShape Shape;
        Shape.SetAsBox((50.0f / 2) * PhysicsScale / 4, (50.f / 2) * PhysicsScale / 4);
        b2FixtureDef FixtureDef;
        FixtureDef.density = 10.0f;
        FixtureDef.friction = 0.8f;
        FixtureDef.restitution = 0.0f;//0.8f;
        FixtureDef.shape = &Shape;
        Body->CreateFixture(&FixtureDef);
        Body->SetFixedRotation(true);
        ennemiesBodies.push_back(Body);
        mp.ennemiesBodiesFromPhysics.push_back(Body);
        ennemiesBodiesInitialPos.push_back(Body->GetPosition());
    }
    //declaration chrono
    int counter = 0;

    void Physics::update(gf::Time time) {

        //pour eviter chute plus rapide quand appuie sur touche saut ou descendre
        if (body->GetGravityScale() > 2) {
            body->SetGravityScale(2);
        }

        for (size_t i = 0; i < fallingBodies.size();i++) {
            mp.cling.at(i)=false;
        }

        //si le joueur meurt ou gagne, les �l�ments du d�cord continuent de bouger
        if (/*body->GetPosition().y >= 20.0f || */player.isFalling) {
            body->SetLinearVelocity({ 0.0f,10.0f });//cesse de tomber dans le vide ou de bouger sur les c�t�s

            //plateformes se d�pla�ant sur l'axe horizontal
            for (size_t i = 0; i < elevatorBodies.size(); i++) {
                if ((float)movingBodies.at(i)->GetPosition().x + 12.5f <= 15.12f) {
                    movingBodies.at(i)->SetLinearVelocity({ 0.2,0 });
                    mp.movingBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.2,0 });
                }
                else if ((float)movingBodies.at(i)->GetPosition().x + 12.5f >= 15.7f) {
                    movingBodies.at(i)->SetLinearVelocity({ -0.2,0 });
                    mp.movingBodiesFromPhysics.at(i)->SetLinearVelocity({ -0.2,0 });
                }
            }

            //ascenseur(s)
            for (size_t i = 0; i < elevatorBodies.size(); i++) {
                if ((float)elevatorBodies.at(i)->GetPosition().y >= elevatorBodiesInitialPos.at(i).y) {
                    elevatorBodies.at(i)->SetLinearVelocity({ 0,0.25 });
                    mp.elevatorBodiesFromPhysics.at(i)->SetLinearVelocity({ 0,0.25 });
                }
                else if ((float)elevatorBodies.at(i)->GetPosition().y <= elevatorBodiesInitialPos.at(i).y + 0.75f) {
                    elevatorBodies.at(i)->SetLinearVelocity({ 0,-0.25 });
                    mp.elevatorBodiesFromPhysics.at(i)->SetLinearVelocity({ 0,-0.25 });
                }
            }

            //ennemis se d�pla�ant sur l'axe horizontal
            for (size_t i = 0; i < ennemiesBodies.size(); i++) {
                if ((float)ennemiesBodies.at(i)->GetPosition().x <= ennemiesBodiesInitialPos.at(i).x) {
                    ennemiesBodies.at(i)->SetLinearVelocity({ 0.25,0 });
                    mp.ennemiesBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.25,0 });
                }
                else if ((float)ennemiesBodies.at(i)->GetPosition().x >= ennemiesBodiesInitialPos.at(i).x + 0.5f) {
                    ennemiesBodies.at(i)->SetLinearVelocity({ -0.25,0 });
                    mp.ennemiesBodiesFromPhysics.at(i)->SetLinearVelocity({ -0.25,0 });
                }
            }

            //chute
            static int32 VelocityIterations = 6;
            static int32 PositionIterations = 2;
            world->Step(time.asSeconds(), VelocityIterations, PositionIterations);

            player.setDynamics({ toVec(body->GetPosition()), toVec(body->GetLinearVelocity()) });

            player.setPlayerBodyFromPhysics(body);//on met � jour le corps physique du joueur dans la classe player
        }
        else {

            for (size_t i = 0; i < fallingBodies.size(); i++) {
                float leftSideBlockX = fallingBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                float rightSideBlockX = fallingBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;
                float topSideBlock = fallingBodies.at(i)->GetPosition().y - (50.f / 2) * PhysicsScale / 2;
                //par rapport � la position sur l'axe des abscisses
                if (body->GetPosition().x <= rightSideBlockX && body->GetPosition().x >= leftSideBlockX) {
                    //printf("pos x joueur entre 2 extremit�s\n");
                    if (body->GetPosition().y <= topSideBlock && body->GetPosition().y >= topSideBlock - 0.1f) {
                        counter++;

                        //clignotement
                        if (counter % 2 == 0) {
                            mp.cling.at(i) = true;
                        }
                        else {
                            mp.cling.at(i)=false;
                        }

                        if (counter == 50) {
                            mp.cling.at(i) = false;
                            fallingBodies.at(i)->SetLinearVelocity({ 0,1.0f });
                            mp.fallingBodiesFromPhysics.at(i)->SetLinearVelocity({ 0,1.0f });
                            counter = 0;
                        }
                        break;
                    }
                    else {
                        mp.cling.at(i) = false;
                        counter = 0;
                    }
                }
            }

            for (size_t i = 0; i < fallingBodies.size(); i++) {
                if (fallingBodies.at(i)->GetPosition().y >= fallingBodiesInitialPos.at(i).y + 2.5f) {
                    fallingBodies.at(i)->SetTransform(fallingBodiesInitialPos.at(i), fallingBodies.at(i)->GetAngle());
                    mp.fallingBodiesFromPhysics.at(i)->SetTransform(fallingBodiesInitialPos.at(i), fallingBodies.at(i)->GetAngle());
                    mp.cling.at(i) = false;

                    fallingBodies.at(i)->SetLinearVelocity({ 0.0f,0.0f });
                    mp.fallingBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.0f,0.0f });
                }
            }

            //replacement du bloc poussable apr�s chute
            for (size_t i = 0; i < pushableBodies.size(); i++) {
                if (pushableBodies.at(i)->GetPosition().y >= pushableBodiesInitialPos.at(i).y+12.0f) {
                    pushableBodies.at(i)->SetTransform(pushableBodiesInitialPos.at(i), pushableBodies.at(i)->GetAngle());
                    mp.pushableBodiesFromPhysics.at(i)->SetTransform(pushableBodiesInitialPos.at(i), pushableBodies.at(i)->GetAngle());

                    pushableBodies.at(i)->SetLinearVelocity({ 0.0f,0.0f });
                    mp.pushableBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.0f,0.0f });
                }
            }

            player.isOnAir = isOnAir;
            //printf("is on air ? %d\n", isOnAir);

            auto dynamics = player.getDynamics();
            body->SetTransform(fromVec(dynamics.position), 0.0f);
            //body->SetLinearVelocity(fromVec(dynamics.velocity));

            //saut
            if (player.isRunning) {
                //si saute en plus de marcher
                if (player.isJumpingPhysics) {
                    printf("numfootContact : %i\n", numFootContacts);
                    //corriger le bug de : se d�placer en continue sur un sol et appuyer sur saut
                    if (!isOnAir) {
                        numFootContacts = 1;
                    }
                    //jumpTimeout = 0;
                    //saut en diagonal, direction est l'orientation du perso (gauche/droite)
                    if (numFootContacts < 1 ) {
                        printf("numFootContacts < 1\n");
                    }
                    /*else if (jumpTimeout > 0) {
                        printf("jumpTimeout > 0\n");
                        if (numFootContacts < 1) {
                            printf("numFootContacts < 1\n");
                        }
                        else if(!player.isOnLadder){
                            body->SetGravityScale(0.5);
                            float whichX;
                            if (player.direction == gf::Direction::Left) {
                                whichX = -0.1f * body->GetMass();
                            }
                            else {
                                whichX = 0.1f * body->GetMass();
                            }
                            float impulse = body->GetMass() * 3;
                            body->ApplyLinearImpulse(b2Vec2(whichX, -impulse), body->GetWorldCenter(), true);
                            jumpTimeout = 1;
                        }
                    }*/
                    else {
                        //ok pour le saut
                        float whichX;
                        if (player.direction == gf::Direction::Left) {
                            whichX = -0.1f * body->GetMass();
                        }
                        else {
                            whichX = 0.1f * body->GetMass();
                        }
                        //body->SetLinearVelocity({ whichX ,-6 });
                        body->SetGravityScale(0.5);
                        player.isJumpingPhysics = false;
                        jumpTimeout = 1;
                        float impulse = body->GetMass() * 2.5f;
                        body->ApplyLinearImpulse(b2Vec2(whichX, -impulse), body->GetWorldCenter(), true);

                    }
                }
                else {
                    //printf("deplacement realise dans player.cc\n");
                    float whichX;
                    if (player.direction == gf::Direction::Left) {
                        whichX = -1.0f;
                    }
                    else {
                        whichX = 1.0f;
                    }
                    //body->ApplyLinearImpulse({ whichX*PhysicsScale,0.0f }, body->GetWorldCenter(), true);
                    //body->SetLinearVelocity({ whichX,-0.001f});
                    body->SetLinearVelocity({ whichX,0.0f });
                    //body->ApplyForce(b2Vec2(impulse, 0), body->GetWorldCenter(),true);
                    //body->SetGravityScale(0.5);

                    bool isOnLadder = false;
                    player.isOnLadder = false;
                    for (size_t i = 0; i < climbedBodies.size(); i++) {
                        float playerX = body->GetPosition().x;
                        float leftSideBlockX = climbedBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                        float rightSideBlockX = climbedBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                        float playerY = body->GetPosition().y;
                        float TopSideBlockY = climbedBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                        float BotSideBlockY = climbedBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                        float PlayerHeight = (110.f) * PhysicsScale / 8;
                        if ((player.isClimbingDown || player.isClimbingTop) && playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY >= TopSideBlockY - PlayerHeight && playerY <= BotSideBlockY) {
                            player.currentAnimation = &player.climbing[0];
                            isOnLadder = true;
                            player.isOnLadder = true;
                            break;
                        }

                        if (playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY >= TopSideBlockY + 0.1f - PlayerHeight && playerY <= BotSideBlockY - 0.1f) {
                            player.currentAnimation = &player.climbing[0];
                            isOnLadder = true;
                            player.isOnLadder = true;
                            break;
                        }
                    }

                    //player.currentAnimation = &player.nomove[0];
                    if (isOnLadder) {
                        isOnAir = false;
                        if (player.isJumpingPhysics) {
                            body->SetGravityScale(0.25);
                        }
                        else {
                            body->SetGravityScale(0);
                        }
                    }
                    else {
                        body->SetGravityScale(0.5);
                    }

                }

            }
            else {

                if (player.isJumpingPhysics) {

                    printf("last ground position : (%f,%f)\n", lastGroundPosition.x, lastGroundPosition.y);
                    printf("body player position : (%f,%f)\n", body->GetPosition().x, body->GetPosition().y);

                    printf("numfootContact : %i\n", numFootContacts);
                    /*if (!isOnAir) {
                        jumpTimeout = 0;
                    }*/
                    if (numFootContacts < 1 && !(body->GetPosition().y<=lastGroundPosition.y+0.2 && body->GetPosition().y >= lastGroundPosition.y - 0.1)){
                        printf("numFootContacts < 1\n");
                    }
                    /*else if (jumpTimeout > 0) {
                        printf("jumpTimeout > 0\n");
                        
                        if (numFootContacts < 1) {//ne pas sauter dans les airs
                            printf("numFootContacts < 1 after jumpTimeout >0\n");
                        }
                        else {
                            float impulse = body->GetMass() * 2;
                            body->ApplyLinearImpulse(b2Vec2(0, -impulse), body->GetWorldCenter(), true);
                        }
                    }*/
                    else {
                        //ok pour le saut
                        //body->SetLinearVelocity({ body->GetLinearVelocity().x ,-6 });
                        body->SetGravityScale(0.5);
                        player.isJumpingPhysics = false;
                        jumpTimeout = 1;//pour �viter le spam de la touche de saut
                        float impulse = body->GetMass() * 2;
                        body->ApplyLinearImpulse(b2Vec2(0, -impulse), body->GetWorldCenter(),true);
                    }
                }

                //isOnLadder empeche une acceleration de chute apres appuie sur touche grimper
                else if (player.isClimbingTop && player.isOnLadder) {
                    size_t currentLadderIndex = 0;
                        for (size_t i = 0; i < climbedBodies.size(); i++) {
                            float playerX = body->GetPosition().x;
                            float leftSideBlockX = climbedBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                            float rightSideBlockX = climbedBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                            float playerY = body->GetPosition().y;
                            float TopSideBlockY = climbedBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                            float BotSideBlockY = climbedBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                            float PlayerHeight = (110.f) * PhysicsScale / 8;
                            if (playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY>=TopSideBlockY- PlayerHeight && playerY<=BotSideBlockY) {
                                player.currentAnimation = &player.climbing[0];
                                body->SetGravityScale(0);
                                body->SetLinearVelocity({ 0.0f,-0.5f });
                                currentLadderIndex = i;
                                break;
                            }
                        }
                        float maxHeight = 0.0f;
                        if (!climbedBodies.empty()) {
                            maxHeight = climbedBodies.at(currentLadderIndex)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                        }
                        for (size_t j = 0; j < climbedBodies.size(); j++) {
                            if (j != currentLadderIndex) {
                                float playerX = body->GetPosition().x;
                                float leftSideBlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                                float rightSideBlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;
                                if (playerX >= leftSideBlockX && playerX <= rightSideBlockX) {
                                    float BlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x;
                                    float BlockX2 = climbedBodies.at(j)->GetPosition().x;
                                    //si 2 blocs �chelles sur la m�me abscisse
                                    if (BlockX2 == BlockX) {
                                        //on v�rifie qu'il n'y a aucun espace entre les �chelles (blocs coll�s)
                                        float TopSideBlock1 = climbedBodies.at(currentLadderIndex)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                                        float BotSideBlock2 = climbedBodies.at(j)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                                        if (TopSideBlock1 <= BotSideBlock2 && TopSideBlock1 >= BotSideBlock2 - 0.0001f) {
                                            float topSideBlock2 = climbedBodies.at(j)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                                            if (maxHeight >= topSideBlock2) {
                                                maxHeight = topSideBlock2;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        float FootplayerY = body->GetPosition().y + (110.f / 2) * PhysicsScale / 8;
                        //printf("footPlayer %f / maxHeight %f\n", FootplayerY, maxHeight);
                        //printf("max height %f / height of ladder 2 %f\n", maxHeight, climbedBodies.at(2)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2);
                        if (FootplayerY-0.05f <= maxHeight) {
                                printf("au bout\n");
                                body->SetLinearVelocity({ 0.0f,0.0f });
                                //body->SetGravityScale(0.5);
                                player.isJumpingPhysics = false;
                                float impulse = body->GetMass();
                                
                                if (FootplayerY>=maxHeight-0.1f) {
                                    body->ApplyLinearImpulse(b2Vec2(0, -impulse), body->GetWorldCenter(), true);
                                    body->SetGravityScale(0.5);
                                }
                        }

                }

                else if (player.isClimbingDown) {

                    //printf("velocity y of player %f\n", player.velocity.y);

                    size_t currentLadderIndex = 0;
                    for (size_t i = 0; i < climbedBodies.size(); i++) {
                        float playerX = body->GetPosition().x;
                        float leftSideBlockX = climbedBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                        float rightSideBlockX = climbedBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                        float playerY = body->GetPosition().y;
                        float TopSideBlockY = climbedBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                        float BotSideBlockY = climbedBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                        float PlayerHeight = (110.f) * PhysicsScale / 8;
                        if (playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY >= TopSideBlockY - PlayerHeight && playerY <= BotSideBlockY) {
                            player.currentAnimation = &player.climbing[0];
                            body->SetGravityScale(0);
                            body->SetLinearVelocity({ 0.0f,0.5f });
                            currentLadderIndex = i;
                            break;
                        }
                    }
                    //printf("descente du bloc %zu\n", currentLadderIndex);
                    float minHeight = 0.0f;
                    if (!climbedBodies.empty()) {
                        minHeight = climbedBodies.at(currentLadderIndex)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                    }
                    for (size_t j = 0; j < climbedBodies.size(); j++) {
                        if (j != currentLadderIndex) {
                            float playerX = body->GetPosition().x;
                            float leftSideBlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                            float rightSideBlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;
                            if (playerX >= leftSideBlockX && playerX <= rightSideBlockX) {
                                float BlockX = climbedBodies.at(currentLadderIndex)->GetPosition().x;
                                float BlockX2 = climbedBodies.at(j)->GetPosition().x;
                                //si 2 blocs �chelles sur la m�me abscisse
                                if (BlockX2 == BlockX) {
                                    //on v�rifie qu'il n'y a aucun espace entre les �chelles (blocs coll�s)
                                    float BotSideBlock1 = climbedBodies.at(currentLadderIndex)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                                    float TopSideBlock2 = climbedBodies.at(j)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2; 
                                    if (TopSideBlock2 >= BotSideBlock1 && TopSideBlock2 <= BotSideBlock1 + 0.0001f) {
                                        float BotSideBlock2 = climbedBodies.at(j)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                                        if (minHeight <= BotSideBlock2) {
                                            minHeight = BotSideBlock2;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    float FootplayerY = body->GetPosition().y + (110.f / 2) * PhysicsScale / 8;
                    //printf("footPlayer %f / maxHeight %f\n", FootplayerY, maxHeight);
                    //printf("max height %f / height of ladder 2 %f\n", maxHeight, climbedBodies.at(2)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2);
                    if (FootplayerY - 0.1f >= minHeight) {
                        printf("au bout en bas\n");
                        body->SetLinearVelocity({ 0.0f,0.0f });
                        //body->SetGravityScale(2);//creeait acceleration chute
                    }
                    else {
                        body->SetGravityScale(0);
                    }

                    //chute + mouvements sur map (application physique, si ignor� alors plus rien ne bouge)
                    /*static int32 VelocityIterations = 8;
                    static int32 PositionIterations = 3;
                    world->Step(time.asSeconds(), VelocityIterations, PositionIterations);

                    player.setDynamics({ toVec(body->GetPosition()), toVec(body->GetLinearVelocity()) });
                    mp.playerCoords = { player.getDynamics().position };*/


                }

                else {
                    bool isOnLadder = false;
                    player.isOnLadder = false;
                    for (size_t i = 0; i < climbedBodies.size(); i++) {
                        float playerX = body->GetPosition().x;
                        float leftSideBlockX = climbedBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                        float rightSideBlockX = climbedBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                        float playerY = body->GetPosition().y;
                        float TopSideBlockY = climbedBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                        float BotSideBlockY = climbedBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                        float PlayerHeight = (110.f) * PhysicsScale / 8;
                        if ((player.isClimbingDown || player.isClimbingTop) && playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY >= TopSideBlockY - PlayerHeight && playerY <= BotSideBlockY) {
                            player.currentAnimation = &player.climbing[0];
                            isOnLadder = true;
                            player.isOnLadder = true;
                            break;
                        }

                        if (playerX >= leftSideBlockX && playerX <= rightSideBlockX && playerY >= TopSideBlockY+0.1f - PlayerHeight && playerY <= BotSideBlockY-0.1f) {
                            player.currentAnimation = &player.climbing[0];
                            isOnLadder = true;
                            player.isOnLadder = true;
                            break;
                        }

                    }

                    //player.currentAnimation = &player.nomove[0];
                    if (isOnLadder && !player.isRunning) {
                        if (player.isJumpingPhysics) {
                            body->SetGravityScale(0.25);
                        }
                        else {
                            body->SetGravityScale(0);
                        }
                    }
                    else {
                        body->SetGravityScale(1);
                    }
                    body->SetLinearVelocity({ 0 ,0 });//si ne bouge plus alors ne monte/descend plus
                    jumpTimeout = 0;
                    //printf("ne bouge plus\n");

                    player.setPlayerBodyFromPhysics(body);//on met � jour le corps physique du joueur dans la classe player

                }
            }

            /*if (player.isRunning && player.climbing) {
                body->SetGravityScale(2);
            }*/

            //plateformes se d�pla�ant sur l'axe horizontal
            for (size_t i = 0; i < movingBodies.size(); i++) {
                if ((float)movingBodies.at(i)->GetPosition().x <= movingBodiesInitialPos.at(i).x) {
                    movingBodies.at(i)->SetLinearVelocity({ 0.25,0 });
                    mp.movingBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.25,0 });
                }
                else if ((float)movingBodies.at(i)->GetPosition().x >= movingBodiesInitialPos.at(i).x+0.75f) {
                    movingBodies.at(i)->SetLinearVelocity({ -0.25,0 });
                    mp.movingBodiesFromPhysics.at(i)->SetLinearVelocity({ -0.25,0 });
                }
            }

            //ascenseur(s)
            for (size_t i = 0; i < elevatorBodies.size(); i++) {
                if ((float)elevatorBodies.at(i)->GetPosition().y <= elevatorBodiesInitialPos.at(i).y) {
                    elevatorBodies.at(i)->SetLinearVelocity({ 0,0.25 });
                    mp.elevatorBodiesFromPhysics.at(i)->SetLinearVelocity({ 0,0.25 });
                }
                else if ((float)elevatorBodies.at(i)->GetPosition().y  >= elevatorBodiesInitialPos.at(i).y+0.75f) {
                    elevatorBodies.at(i)->SetLinearVelocity({ 0,-0.25 });
                    mp.elevatorBodiesFromPhysics.at(i)->SetLinearVelocity({ 0,-0.25 });
                }
            }

            //ennemis se d�pla�ant sur l'axe horizontal
            for (size_t i = 0; i < ennemiesBodies.size(); i++) {
                if ((float)ennemiesBodies.at(i)->GetPosition().x <= ennemiesBodiesInitialPos.at(i).x) {
                    ennemiesBodies.at(i)->SetLinearVelocity({ 0.25,0 });
                    mp.ennemiesBodiesFromPhysics.at(i)->SetLinearVelocity({ 0.25,0 });
                }
                else if ((float)ennemiesBodies.at(i)->GetPosition().x >= ennemiesBodiesInitialPos.at(i).x+0.5f) {
                    ennemiesBodies.at(i)->SetLinearVelocity({ -0.25,0 });
                    mp.ennemiesBodiesFromPhysics.at(i)->SetLinearVelocity({ -0.25,0 });
                }
            }
            for (size_t i = 0; i < ennemiesBodies.size(); i++) {
                float playerX = body->GetPosition().x;
                float leftSideBlockX = ennemiesBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                float rightSideBlockX = ennemiesBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                float playerY = body->GetPosition().y;
                float TopSideBlockY = ennemiesBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                float BotSideBlockY = ennemiesBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                if (playerX >= leftSideBlockX-0.01f && playerX <= rightSideBlockX+0.01f && playerY >= TopSideBlockY && playerY <= BotSideBlockY) {
                    player.currentAnimation = &player.death;
                    body->SetLinearVelocity({ 0.0f,0.5f });
                    body->SetType(b2_kinematicBody);
                    player.isFalling = true;
                }
            }

            //scie circulaire
            for (size_t i = 0; i < trapBodies.size(); i++) {
                float playerX = body->GetPosition().x;
                float leftSideBlockX = trapBodies.at(i)->GetPosition().x - (50.0f / 2) * PhysicsScale / 2;
                float rightSideBlockX = trapBodies.at(i)->GetPosition().x + (50.0f / 2) * PhysicsScale / 2;

                float playerY = body->GetPosition().y;
                float TopSideBlockY = trapBodies.at(i)->GetPosition().y - (50.0f / 2) * PhysicsScale / 2;
                float BotSideBlockY = trapBodies.at(i)->GetPosition().y + (50.0f / 2) * PhysicsScale / 2;
                if (playerX >= leftSideBlockX-0.08f && playerX <= rightSideBlockX+0.08f && playerY >= TopSideBlockY && playerY <= BotSideBlockY) {
                    player.currentAnimation = &player.death;
                    body->SetLinearVelocity({ 0.0f,5.0f });
                    body->SetType(b2_kinematicBody);
                    player.isFalling = true;
                }
            }

            //chute + mouvements sur map (application physique, si ignor� alors plus rien ne bouge)
            static int32 VelocityIterations = 8;
            static int32 PositionIterations = 3;
            world->Step(time.asSeconds(), VelocityIterations, PositionIterations);

            player.setDynamics({ toVec(body->GetPosition()), toVec(body->GetLinearVelocity()) });
            mp.playerCoords = { player.getDynamics().position };

            //printf("current gravity scale : %f\n", body->GetGravityScale());
            //printf("current gravity y in world : %f\n", world->GetGravity().y);

        }

    }

}