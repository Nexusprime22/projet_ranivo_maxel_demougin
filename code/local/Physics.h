#ifndef PLATEFORMER2021_PHYSICS_H
#define PLATEFORMER2021_PHYSICS_H
#include <vector>

#include <box2d/box2d.h>

#include <gf/Circ.h>
#include <gf/Entity.h>
#include <gf/Model.h>
#include <gf/Polygon.h>
#include <gf/Tmx.h>

#include "Player.h"
#include "MapScene.h"
#include "Procedural.h"

namespace plateformer2021 {

    class MyContactListener : public b2ContactListener {
    public:
        virtual void BeginContact(b2Contact* contact);
        // fonction appel�e quand 2 bodies cessent de se toucher
        virtual void EndContact(b2Contact* contact);
    };

    class Physics : public gf::Model {
    public:

        int jumpTimeout;//pour emp�cher le saut apr�s un saut

        b2World* world;
        b2Vec2 gravity;
        b2Body* body;
        Player& player;
        MapScene& mp;

        std::vector<b2Body*> staticBodies;//sol
        std::vector<b2Body*> wallBodies;//mur

        std::vector<b2Body*> movingBodies;
        std::vector<b2Vec2> movingBodiesInitialPos;

        std::vector<b2Body*> elevatorBodies;
        std::vector<b2Vec2> elevatorBodiesInitialPos;

        std::vector<b2Body*> pushableBodies;
        std::vector<b2Vec2> pushableBodiesInitialPos;

        std::vector<b2Body*> fallingBodies;
        std::vector<b2Vec2> fallingBodiesInitialPos;

        std::vector<b2Body*> climbedBodies;

        std::vector<b2Body*> trapBodies;

        std::vector<b2Body*> ennemiesBodies;
        std::vector<b2Vec2> ennemiesBodiesInitialPos;

        Physics(Player& player,MapScene& mp);

        void generateTutorialPhysicalWorld();
        void generatePhysicalWorld();

        /*void bodyJump();*/

        void createInvisibleWallLeft();
        void createInvisibleWallRight();

        void createInvisibleWallTop();

        void createStaticGround(float X, float Y);
        void createStaticWall(float X, float Y);

        void createFallingGround(float X, float Y);
        void createElevatorGround(float X, float Y);
        void createMovingGround(float X, float Y);
        void createPushableObject(float X, float Y);
        void createClimbedBlock(float X, float Y);

        void createEnnemy(float X, float Y);

        void createTrapBlock(float X, float Y);

        virtual void update(gf::Time time) override;

        void createPhysicalPlayer();

    };
    
}
#endif