#ifndef PLATEFORMER2021_VICTOIRE_H
#define PLATEFORMER2021_VICTOIRE_H


#include <gf/Entity.h>
#include <gf/Texture.h>

namespace plateformer2021 {

  class Victoire : public gf::Entity {
  public:

      gf::Vector2f positionFin;
      const gf::Texture& m_texture;

      Victoire();

    std::string aleaScreenVictoire();
    void setPositionFin(float x, float y);

    virtual void render(gf::RenderTarget& target, const gf::RenderStates& states) override;
  };

}

#endif // PLATEFORMER2021_VICTOIRE_H
