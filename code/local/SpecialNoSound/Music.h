#ifndef PLATEFORMER2021_MUSIC_H
#define PLATEFORMER2021_MUSIC_H

#include <iostream>
#include <cstdlib>
#include <cassert>
#include <gf/Entity.h>
#include <SFML/Audio.hpp>

namespace plateformer2021 {

    class Music {
    public:
        Music();

        void mute();

    private:
        sf::Music& backgroundMusic;
    };

}

#endif