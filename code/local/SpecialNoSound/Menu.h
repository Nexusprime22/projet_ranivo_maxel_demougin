#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/Entity.h>

#include <iostream>
#include <cstdlib>
#include <cassert>

#include <box2d/box2d.h>

namespace plateformer2021 {
    class Menu{
      public:

        Menu();
		int printMenu();//0 menu principal, 1 jeu, autre val pour bonus

    };
}