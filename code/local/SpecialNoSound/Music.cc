/*

    MAXEL Killian TP1A
    DEMOUGIN Alexandre TP1A
    RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/RenderTarget.h>
#include <gf/ResourceManager.h>
#include <gf/MessageManager.h>
#include <gf/Random.h>
#include <gf/Coordinates.h>
#include <gf/Log.h>

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "config.h"

#include <gf/RenderTarget.h>
#include <gf/Singleton.h>
#include <gf/Unused.h>
#include <gf/Message.h>
#include <gf/Collision.h>
#include <gf/TileLayer.h>

#include "Entity.h"
#include "MapScene.h"
#include "Singletons.h"
#include "Messages.h"

#include "box2d/box2d.h"

#include "Player.h"

#include <gf/Log.h>
#include <gf/RenderTarget.h>
#include <gf/Shapes.h>
#include <gf/Tmx.h>
#include <gf/TmxOps.h>

#include "Music.h"
#include <SFML/Audio.hpp>

static constexpr float DefaultMusicVolume = 2.0f;

namespace plateformer2021 {
    Music::Music() : backgroundMusic(rsc().getMusic("sounds/main_theme.ogg"))
    {
        backgroundMusic.setLoop(true);
        backgroundMusic.setVolume(DefaultMusicVolume);
        backgroundMusic.play();

    }

    void Music::mute() {
        if (backgroundMusic.getStatus() == sf::SoundSource::Playing) {
            backgroundMusic.pause();
        }
        else {
            backgroundMusic.play();
        }
    }

}