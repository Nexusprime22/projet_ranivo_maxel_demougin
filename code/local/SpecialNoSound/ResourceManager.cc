#include "ResourceManager.h"

namespace plateformer2021 {

    namespace {
        template<typename T>
        class ResourceLoader {
        public:
            std::unique_ptr<T> operator()(const gf::Path& filename) {
                auto ptr = std::make_unique<T>();
                bool loaded = ptr->loadFromFile(filename.string());
                return loaded ? std::move(ptr) : nullptr;
            }
        };
    }

    ResourceManager::ResourceManager()
    {

    }

}
