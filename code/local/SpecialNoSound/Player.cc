#include <gf/AnimatedSprite.h>
#include <gf/RenderTarget.h>
#include <gf/Unused.h>
#include <gf/VectorOps.h>
#include <gf/Entity.h>

#include "MapScene.h"
#include "Messages.h"
#include "Singletons.h"
#include "Player.h"

#include <box2d/box2d.h>

#include "Victoire.h"
#include "Defaite.h"

namespace plateformer2021 {

    static constexpr float TextureHeight = 110.0f;//hauteur
    static constexpr float TextureWidth = 80.0f;//largeur

    //static constexpr gf::Time FrameTime = gf::milliseconds(60);

    static constexpr float Speed = 80.0f;//vitesse du joueur

    static void loadSingleFrameAnimation(gf::Animation& animation, const gf::Path& path) {
        gf::Texture& texture = rsc().getTexture(path);
        texture.setSmooth();
        //on cr�e un rectangle � la position (0,0) de taille (1,1)
        animation.addFrame(texture, gf::RectF::fromPositionSize({ 0.0f, 0.0f }, { 1.0f, 1.0f }), gf::seconds(1));
    }

    static void loadMultiFrameAnimation(gf::Animation& animation, const gf::Path& path) {
        gf::Texture& texture = rsc().getTexture(path);
        texture.setSmooth();
    }

    Player::Player()
        :gf::Entity(100)//2 correspond � la priorit� de render de l'entit�, plus elle est haute plus le render se fait rapidement
        , initialPosition(50, 200)
        , position(50, 200)
        , direction(gf::Direction::Right)
        , isRunning(false)
        , isJumping(false)
        , isClimbingTop(false)
        , isClimbingDown(false)
        , currentAnimation(&nomove[0])//d�bute avec l'animation stand et orient� � droite
        , playerBody(NULL)
        , win(false)
        , isFalling(false)
        , positionBeforeDeath(50,200)
    {

        // chargement des animations

        loadSingleFrameAnimation(nomove[0], "texture/Player/player_stand_right.png");
        loadSingleFrameAnimation(nomove[1], "texture/Player/player_stand_left.png");

        loadSingleFrameAnimation(running[0], "texture/Player/player_walk1_right.png");
        loadSingleFrameAnimation(running[1], "texture/Player/player_walk2_right.png");
        loadSingleFrameAnimation(running[2], "texture/Player/player_walk1_left.png");
        loadSingleFrameAnimation(running[3], "texture/Player/player_walk2_left.png");

        loadSingleFrameAnimation(jumping[0], "texture/Player/player_jump_right.png");
        loadSingleFrameAnimation(jumping[1], "texture/Player/player_jump_left.png");

        loadSingleFrameAnimation(climbing[0], "texture/Player/player_climb1.png");
        loadSingleFrameAnimation(climbing[1], "texture/Player/player_climb1.png");

        loadSingleFrameAnimation(death, "texture/Player/player_hurt.png");

        loadSingleFrameAnimation(victory, "texture/Player/player_cheer1.png");

        // register messages

        msgMnger().registerHandler<NextMapMessage>(&Player::onNewMap, this);
        msgMnger().registerHandler<ResetMapMessage>(&Player::onResetMap, this);
    }

    void Player::setPlayerBodyFromPhysics(b2Body* bodyOfPl) {
        playerBody = bodyOfPl;
    }

    static gf::RectF boundsFromPosition(gf::Vector2f position) {
        return gf::RectF::fromPositionSize( { position.x- MapScene::TileSize , position.y + MapScene::TileSize }, { MapScene::TileSize/2 , MapScene::TileSize/2 } );
    }

    void Player::update(gf::Time time) {

        if (win) {
            currentAnimation = &victory;
        }

        if (!win && !isFalling) {

            float dt = time.asSeconds();
                // update position
                if (isRunning) {
                    prevPosition = position;
                    //si saute en plus de marcher
                    gf::Vector2f nextPos = position + gf::displacement(direction) * Speed * dt;
                    gf::Vector2f move = nextPos - position;
                    float length = gf::euclideanLength(move);
                    if (isJumping) {
                        velocity = (move / length) * Speed;
                        position += gf::displacement(direction) * Speed * dt / 2;
                    }
                    else {
                        velocity = (move / length) * Speed;
                        position += gf::displacement(direction) * Speed * dt / 2;
                        if (direction == gf::Direction::Left) {
                            position.x = position.x - 3.0f;
                        }
                        else {
                            position.x = position.x + 3.0f;
                        }
                    }
                }
                else {
                    //si immobile mais saute
                    //velocity = { 0.0f, 0.0f };
                }

                //printf("%i\n", countJump);

                //les assert sous windows ne fonctionnent pas
                int dir = static_cast<int>(direction);
                assert(currentAnimation);
                assert(0 <= dir && dir <= 3);//pour s'assurer qu'on ne d�passe pas les indices du tableau d'animations
                int dirRun = static_cast<int>(direction);
                assert(0 <= dirRun && dirRun < 4);

                if (isRunning) {
                    if (isJumping) {
                        if (dir == 3) {
                            currentAnimation = &jumping[1];
                            isJumping = false;
                        }
                        else {
                            currentAnimation = &jumping[0];
                            isJumping = false;
                        }
                    }
                    else {
                        currentAnimation = &running[dirRun - 1];
                    }
                }
                else {
                    if (isJumping) {
                        if (dir == 3) {
                            currentAnimation = &jumping[1];
                            isJumping = false;
                        }
                        else {
                            currentAnimation = &jumping[0];
                            isJumping = false;
                        }
                    }
                    else {
                        if (dir == 3) {//si gauche
                            currentAnimation = &nomove[1];
                        }
                        else {
                            currentAnimation = &nomove[0];
                        }
                    }
                }
                /*if (isClimbingTop || isClimbingDown) {
                    currentAnimation = &climbing[0];
                }*/

                currentAnimation->update(time);

                // send message

                PlayerPositionMessage msg;
                msg.position = position;
                msg.bounds = boundsFromPosition(position);
                msg.isRunning = isRunning;
                msgMnger().sendMessage(&msg);

                position = msg.position;

                positionBeforeDeath = position;
            //}
        }

    }

    void Player::render(gf::RenderTarget& target, const gf::RenderStates& states) {

        /*gf::Sprite repere;
        repere.setTexture(rsc().getTexture("texture/Player/player_stand_right.png"));
        repere.setScale({ 0.25 });
        repere.setPosition({initialPosition.x,initialPosition.y-5.0f});
        target.draw(repere,states);*/

        gf::AnimatedSprite sprite;
        sprite.setAnimation(*currentAnimation);
        sprite.setScale({ 0.25 });//penser � /4 dans physics
        //sprite.setScale({ MapScene::TileSize / TextureWidth, MapScene::TileSize / TextureHeight });
        sprite.setPosition({ position.x, position.y-5.0f });
        

        gf::Vector2f endMapFlagPos = { endFlagPos.x * 100 - 4.0f, endFlagPos.y * 100 + 6.2f };
        /*printf("flag(%f,%f)\n", endMapFlagPos.x, endMapFlagPos.y);
        printf("player(%f,%f)\n", position.x, position.y);*/
        if (win || (position.x <= endMapFlagPos.x + 6.0f && position.x >= endMapFlagPos.x-6.0f && position.y <= endMapFlagPos.y + 3.0f && position.y >= endMapFlagPos.y - 3.0f)) {
            win = true;
            plateformer2021::Victoire victory;
            victory.setPositionFin(position.x, position.y);
            victory.render(target, states);
        }
        
        sprite.setAnchor(gf::Anchor::Center);
        target.draw(sprite, states);

        if (isFalling) {
            Defaite defeat;
            defeat.setPositionMort(positionBeforeDeath.x,positionBeforeDeath.y);
            defeat.render(target, states);
        }

    }

    gf::MessageStatus Player::onNewMap(gf::Id id, gf::Message* msg) {
        gf::unused(id);

        auto newMap = static_cast<NextMapMessage*>(msg);
        position = newMap->playerPosition;
        initialPosition = position;
        return gf::MessageStatus::Keep;
    }

    gf::MessageStatus Player::onResetMap(gf::Id id, gf::Message* msg) {
        gf::unused(id);
        gf::unused(msg);

        position = initialPosition;
        return gf::MessageStatus::Keep;
    }

}