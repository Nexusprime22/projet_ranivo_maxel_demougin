/*

    MAXEL Killian TP1A
    DEMOUGIN Alexandre TP1A
    RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/Unused.h>

#include "Menu.h"
#include "Physics.h"

#include "../../build/config.h"

//#include <gf/ResourceManager.h>
//#include <gf/MessageManager.h>
#include <gf/Random.h>
#include <gf/Singleton.h>

#include "MapScene.h"
#include "Singletons.h"
#include "Messages.h"
#include "Player.h"
#include "PhysicsDebugger.h"//pour avoir un affichage des objets physiques

#include <iostream>
#include <cstdlib>
#include <cassert>
#include <box2d/box2d.h>
#include <gf/ModelContainer.h>
#include <gf/Log.h>

namespace plateformer2021{

    Menu::Menu()
    {
    }

    Player reinitPlayer(Player pl) {
        //pour empecher que le perso soit toujours consid�r� au sol de victoire m�me � pos diff�rente
        if (pl.win) {
            pl.playerBody->SetLinearVelocity({0.0f,0.01f});
            pl.setPlayerBodyFromPhysics(pl.playerBody);
        }

        if (pl.isFalling || pl.win==false) {
            pl.playerBody = nullptr;
        }
        pl.position = pl.initialPosition;
        pl.isFalling = false;
        pl.direction = gf::Direction::Right;
        pl.isRunning = false;
        pl.isJumping = false;
        pl.isClimbingTop = false;
        pl.isClimbingDown = false;
        pl.countJump = 0;
        pl.win = false;
        pl.currentAnimation = &pl.nomove[0];
        pl.positionBeforeDeath = pl.initialPosition;
        return pl;
    }

    void supprVectorsFromPhysics(Physics phys, MapScene map) {
        printf("number of bodies in box2dWorld before erasing : %i\n", phys.world->GetBodyCount());
        printf("number of elements in staticbodies before clear : %zu\n", phys.staticBodies.size());
        printf("number of elements in staticbodies in map before clear : %zu\n", map.staticBodiesFromPhysics.size());
        printf("capacity in staticbodies before clear : %zu\n", phys.staticBodies.capacity());
        printf("capacity in staticbodies in map before clear : %zu\n\n", map.staticBodiesFromPhysics.capacity());

        phys.climbedBodies.clear();
        phys.movingBodies.clear();
        phys.elevatorBodies.clear();
        phys.fallingBodies.clear();
        phys.pushableBodies.clear();
        phys.staticBodies.clear();
        phys.trapBodies.clear();
        phys.ennemiesBodies.clear();
        phys.wallBodies.clear();

        map.climbedBodiesFromPhysics.clear();
        map.fallingBodiesFromPhysics.clear();
        map.cling.clear();
        map.fallingBodiesInitialPosFromPhysics.clear();
        map.movingBodiesFromPhysics.clear();
        map.elevatorBodiesFromPhysics.clear();
        map.pushableBodiesFromPhysics.clear();
        map.staticBodiesFromPhysics.clear();
        map.trapBodiesFromPhysics.clear();
        map.ennemiesBodiesFromPhysics.clear();
        map.wallBodiesFromPhysics.clear();

            phys.pushableBodiesInitialPos.clear();
            phys.fallingBodiesInitialPos.clear();
            phys.ennemiesBodiesInitialPos.clear();


            b2Body* body = phys.world->GetBodyList();
            while (body) {
                b2Body* b = body;
                body = body->GetNext();
                phys.world->DestroyBody(b);
            }

            printf("number of bodies in box2dWorld after erasing : %i\n", phys.world->GetBodyCount());
            printf("number of elements in staticbodies in phys after clear : %zu\n", phys.staticBodies.size());
            printf("number of elements in staticbodies in map after clear : %zu\n", map.staticBodiesFromPhysics.size());
            printf("capacity in staticbodies after clear : %zu\n", phys.staticBodies.capacity());
            printf("capacity in staticbodies in map after clear : %zu\n\n", map.staticBodiesFromPhysics.capacity());

    }

    int Menu::printMenu() {

        static constexpr gf::Vector2u ScreenSize(1024, 576);
        static constexpr gf::Vector2i ViewSize(200.0f, 200.0f);
        static constexpr gf::Vector2i ViewCenter(0.0f, 0.0f);

        gf::SingletonStorage<gf::MessageManager> storageForMessageManager(plateformer2021::msgMnger);
        gf::SingletonStorage<ResourceManager> storageForResourceManager(plateformer2021::rsc);

        rsc().addSearchDir(PLATEFORMER2021_DATA_DIR);

        // initialization
        gf::Window window("Plateformer2021", ScreenSize);
        window.setVerticalSyncEnabled(true);
        window.setFramerateLimit(60);
        gf::RenderWindow renderer(window);

        // Load an image file from a file

        gf::Path pathmenu = "texture/menu.png";

        gf::Texture& texture= rsc().getTexture(pathmenu);
        gf::Sprite sprite;
        sprite.setTexture(texture);

        gf::Path pathchoice = "texture/choice.png";

        gf::Texture& texture2= rsc().getTexture(pathchoice);
        gf::Sprite sprite2;
        sprite2.setTexture(texture2);
        sprite2.setPosition({ 550,410 });//gauche-droite/haut-bas

        // Load text

        gf::Path pathFont = "texture/impact.ttf";
        gf::Font& font=rsc().getFont(pathFont);
        gf::Text text("Play", font, 50);
        text.setPosition({ 465,450 });

        // cursor

        // views
        gf::ViewContainer views;
        gf::ExtendView mainView(ViewCenter, ViewSize);
        views.addView(mainView);
        gf::ScreenView hudView;
        views.addView(hudView);
        views.setInitialFramebufferSize(ScreenSize);
        //views.setInitialScreenSize(ScreenSize);

        gf::Vector2f pos = sprite.getPosition();//position du menu

        // actions
        gf::ActionContainer actions;

        gf::Action closeWindowAction("Close window");
        closeWindowAction.addCloseControl();
        closeWindowAction.addKeycodeKeyControl(gf::Keycode::Escape);
        actions.addAction(closeWindowAction);

        gf::Action fullscreenAction("Fullscreen");
        fullscreenAction.addKeycodeKeyControl(gf::Keycode::F);
        actions.addAction(fullscreenAction);

        gf::Action okAction("Ok");
        okAction.addKeycodeKeyControl(gf::Keycode::Return);
        okAction.setInstantaneous();
        actions.addAction(okAction);

        gf::Action upAction("Up");
        upAction.addScancodeKeyControl(gf::Scancode::W);
        upAction.addScancodeKeyControl(gf::Scancode::Up);
        upAction.setInstantaneous();
        actions.addAction(upAction);

        gf::Action downAction("Down");
        downAction.addScancodeKeyControl(gf::Scancode::S);
        downAction.addScancodeKeyControl(gf::Scancode::Down);
        downAction.setInstantaneous();
        actions.addAction(downAction);

        //touches du jeu

        gf::ActionContainer actionsGame;

        gf::Action okAction2("Ok2");
        okAction2.addKeycodeKeyControl(gf::Keycode::Return);
        okAction2.setContinuous();
        actionsGame.addAction(okAction2);

        gf::Action leftAction("Left");
        leftAction.addScancodeKeyControl(gf::Scancode::A);
        leftAction.addScancodeKeyControl(gf::Scancode::Left);
        leftAction.setContinuous();
        actionsGame.addAction(leftAction);

        gf::Action rightAction("Right");
        rightAction.addScancodeKeyControl(gf::Scancode::D);
        rightAction.addScancodeKeyControl(gf::Scancode::Right);
        rightAction.setContinuous();
        actionsGame.addAction(rightAction);

        gf::Action upClimbAction("UpClimb");
        upClimbAction.addScancodeKeyControl(gf::Scancode::Z);
        upClimbAction.addScancodeKeyControl(gf::Scancode::Up);
        upClimbAction.setContinuous();
        actionsGame.addAction(upClimbAction);

        gf::Action downClimbAction("DownClimb");
        downClimbAction.addScancodeKeyControl(gf::Scancode::S);
        downClimbAction.addScancodeKeyControl(gf::Scancode::Down);
        downClimbAction.setContinuous();
        actionsGame.addAction(downClimbAction);

        gf::Action jumpAction("Space");
        jumpAction.addScancodeKeyControl(gf::Scancode::W);
        jumpAction.addScancodeKeyControl(gf::Scancode::Space);
        jumpAction.setContinuous();
        actionsGame.addAction(jumpAction);

        gf::Action printPhysics("PhysicsDebugger");
        printPhysics.addScancodeKeyControl(gf::Scancode::P);
        printPhysics.setInstantaneous();
        actionsGame.addAction(printPhysics);

        // entities
        gf::EntityContainer mainEntities;

        // add entities to mainEntities
        gf::EntityContainer hudEntities;
        // add entities to hudEntities

        // game loop
        renderer.clear(gf::Color::Cyan);
        gf::Clock clock;
        int menu = 1;        

        //ELEMENTS DU JEU

        gf::EntityContainer gameEntities;

        float scaleX = (float)renderer.getSize().width / 1024;
        float scaleY = (float)renderer.getSize().height / 576;

        plateformer2021::Player pl;

        plateformer2021::msgMnger().registerHandler<plateformer2021::PlayerPositionMessage>([&mainView](gf::Id id, gf::Message* msg) {
            assert(id == plateformer2021::PlayerPositionMessage::type);
            gf::unused(id);
            auto plPosition = static_cast<plateformer2021::PlayerPositionMessage*>(msg);
            mainView.setCenter(plPosition->position);
            return gf::MessageStatus::Keep;
        });

        /*gf::Path pathBckgrnd = "texture/background.png";

        gf::Texture& textureBckgrnd=rsc().getTexture(pathBckgrnd);
        gf::Sprite spriteBckgrnd;
        spriteBckgrnd.setTexture(textureBckgrnd);
        spriteBckgrnd.setPosition({ 1 * scaleX,1 * scaleY });
        spriteBckgrnd.setScale({ scaleX,scaleY });*/

        //affichage de map et physique

        plateformer2021::MapScene mp(pl);

        gameEntities.addEntity(mp);
        gameEntities.addEntity(pl);

        gf::ModelContainer models;
        Physics physics(pl,mp);
        models.addModel(physics);

        //physics debugger

        plateformer2021::PhysicsDebugger debug(physics);
        gameEntities.addEntity(debug);
        bool boolDebug = true;

        window.setMouseCursorVisible(false);
		
        while (window.isOpen()) {

            /*printf("texture (%i,%i)\n", pl.position.x, pl.position.y);
            printf("physical body (%i,%i)\n", physics.body->GetPosition().x, physics.body->GetPosition().y);
            printf("physical player (%i,%i)\n", physics.player.position.x, physics.player.position.y);*/

            scaleX = (float)renderer.getSize().width / 1024;
            scaleY = (float)renderer.getSize().height / 576;

            /*printf("(%f)\n", (float)renderer.getSize().height/576);
            printf("(%f)\n", (float)renderer.getSize().width/ 1024);
            printf("%i", renderer.getSize().height);
            printf("(%i", window.getFramebufferSize().height);//meme valeur que ci dessus
            printf(",%i)\n",window.getFramebufferSize().width);*/

            if (menu == 1) {

                // 1. input
                gf::Event event;
                while (window.pollEvent(event)) {
                    actions.processEvent(event);
                    views.processEvent(event);
                }
                if (closeWindowAction.isActive()) {
                    window.close();
                }
                if (fullscreenAction.isActive()) {
                    window.toggleFullscreen();
                    if (!window.isFullscreen()) {
                        scaleX = 1;
                        scaleY = 1;
                    }
                    else {
                        scaleX = 2;
                        scaleY = 2;
                    }
                }
                if (upAction.isActive() || downAction.isActive()) {
                }
                if (upAction.isActive() && text.getString()=="Tuto") {
                    text.setString("Play");
                }
                else if (downAction.isActive() && text.getString()=="Play") {
                    text.setString("Tuto");
                }
                else if (upAction.isActive() && text.getString() == "Leave") {
                    text.setString("Tuto");
                }
                else if (downAction.isActive() && text.getString() == "Tuto") {
                    text.setString("Leave");
                }
                else if (okAction.isActive() && text.getString() == "Leave") {
                    window.close();
                }
                else if (okAction.isActive() && text.getString() == "Play") {
                    //launch game
                    menu = 0;

                    printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

                        pl = reinitPlayer(pl);
                        physics.generatePhysicalWorld();
                        pl = reinitPlayer(pl);
                        physics.createPhysicalPlayer();

                        printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

                    static constexpr gf::Vector2i ProceduralViewSize(200.0f, 200.0f);
                    mainView.setSize(ProceduralViewSize);
                }
                else if (okAction.isActive() && text.getString() == "Tuto") {
                    //launch game
                    pl = reinitPlayer(pl);
                    physics.player = pl;
                    menu = 0;

                        physics.createPhysicalPlayer();
                        physics.generateTutorialPhysicalWorld();

                    static constexpr gf::Vector2i TutorialViewSize(200.0f, 200.0f);
                    mainView.setSize(TutorialViewSize);
                }
                else {
                    // do something
                }

                // 2. update
                gf::Time time = clock.restart();
                mainEntities.update(time);
                hudEntities.update(time);

                sprite.setScale({ scaleX,scaleY });
                sprite2.setScale({ scaleX,scaleY });
                text.setScale({ scaleX,scaleY });
                if (text.getString() == "Play") {
                    sprite2.setPosition({ 550 * scaleX,410 * scaleY });
                }
                else {
                    sprite2.setPosition({ 580 * scaleX,410 * scaleY });
                }
                text.setPosition({ 465 * scaleX, 450 * scaleY });

                // 3. draw
                renderer.clear();
                renderer.draw(sprite);
                renderer.draw(text);
                renderer.draw(sprite2);
                renderer.setView(mainView);
                mainEntities.render(renderer);
                renderer.setView(hudView);
                hudEntities.render(renderer);
                renderer.display();
                actions.reset();
            }

            if (menu == 0) {

                //JEU

                //printf("(%f,%f)\n", pl.position.x, pl.position.y);
                
                // 1. input
                gf::Event event;
                while (window.pollEvent(event)) {
                    actions.processEvent(event);
                    actionsGame.processEvent(event);
                    views.processEvent(event);
                }

                if (pl.isFalling && okAction2.isActive()) {
                    pl = reinitPlayer(pl);
                    physics.player = pl;
                    mp.player = pl;
                    mp.playerP = &pl;
                    menu = 1;
                    supprVectorsFromPhysics(physics, mp);
                    //map r�initialis�e ou tout recommencer ?
                }
                if (pl.win && okAction2.isActive()) {
                    pl = reinitPlayer(pl);
                    //map suivante
                }

                if (printPhysics.isActive()) {
                    //printf("Print physics\n");
                    if (boolDebug) {
                        boolDebug = false;
                    }
                    else {
                        boolDebug = true;
                    }
                    debug.setDebug(boolDebug);
                }

                if (!pl.win && !pl.isFalling) {
                    if (upClimbAction.isActive() && jumpAction.isActive() && leftAction.isActive()) {
                        pl.Jump();
                        pl.goLeft();
                        pl.ClimbUp();
                    }
                    else if (downClimbAction.isActive() && jumpAction.isActive() && leftAction.isActive()) {
                        pl.Jump();
                        pl.goLeft();
                        pl.ClimbDown();
                    }
                    else if (upClimbAction.isActive() && jumpAction.isActive() && rightAction.isActive()) {
                        pl.Jump();
                        pl.goRight();
                        pl.ClimbUp();
                    }
                    else if (downClimbAction.isActive() && jumpAction.isActive() && rightAction.isActive()) {
                        pl.Jump();
                        pl.goRight();
                        pl.ClimbDown();
                    }
                    else if (rightAction.isActive() && upClimbAction.isActive()) {
                        pl.goRight();
                        physics.body->SetGravityScale(2);
                        pl.ClimbUp();
                    }
                    else if (rightAction.isActive() && downClimbAction.isActive()) {
                        pl.goRight();
                        physics.body->SetGravityScale(2);
                        pl.ClimbDown();
                    }
                    else if (leftAction.isActive() && upClimbAction.isActive()) {
                        pl.goLeft();
                        physics.body->SetGravityScale(2);
                        pl.ClimbUp();
                    }
                    else if (leftAction.isActive() && downClimbAction.isActive()) {
                        pl.goLeft();
                        physics.body->SetGravityScale(2);
                        pl.ClimbDown();
                    }
                    else if (rightAction.isActive() && jumpAction.isActive()) {
                        pl.Jump();
                        pl.goRight();
                    }
                    else if (upClimbAction.isActive() && jumpAction.isActive()) {
                        pl.Jump();
                        pl.ClimbUp();
                    }
                    else if (downClimbAction.isActive() && jumpAction.isActive()) {
                        pl.Jump();
                        pl.ClimbDown();
                    }
                    else if (leftAction.isActive() && jumpAction.isActive()) {
                        pl.Jump();
                        pl.goLeft();
                    }
                    else if (leftAction.isActive()) {
                        pl.goLeft();
                    }
                    else if (rightAction.isActive()) {
                        pl.goRight();
                    }
                    else if (jumpAction.isActive()) {
                        pl.Jump();
                        //physics.bodyJump();
                        //printf("(%f,%f)\n", pl.playerBody->GetPosition().x, pl.playerBody->GetPosition().y);
                        //printf("(%f,%f)\n", pl.position.x, pl.position.y);
                    }
                    else if (upClimbAction.isActive()/*&& persoSurEchelle*/) {
                        pl.ClimbUp();
                        //monte
                        physics.player = pl;
                        mp.player = pl;
                        mp.playerP = &pl;
                    }
                    else if (downClimbAction.isActive()) {
                        //printf("Climbing down before ? %d", pl.isClimbingDown);
                        pl.ClimbDown();
                        //descend
                        //printf("Climbing down after ? %d", pl.isClimbingDown);
                        physics.player = pl;
                        mp.player = pl;
                        mp.playerP = &pl;
                    }
                    else {
                        pl.stop();
                    }
                }

                if (closeWindowAction.isActive()) {
                    supprVectorsFromPhysics(physics,mp);
                    text.setString(text.getString());
                    if (!window.isFullscreen()) {
                        text.setPosition({ 465 * scaleX,450 * scaleY });
                    }
                    else {
                        text.setPosition({ 955,900 });
                    }
                    text.setCharacterSize(50);
                    menu = 1;
                }
                if (fullscreenAction.isActive()) {
                    window.toggleFullscreen();
                    if (!window.isFullscreen()) {
                        scaleX = 1;
                        scaleY = 1;
                    }
                    else {
                        scaleX = 2;
                        scaleY = 2;
                    }
                }

                // 2. update
                gf::Time time = clock.restart();
                gameEntities.update(time);
                hudEntities.update(time);
                models.update(time);

                // 3. draw
                renderer.clear();
                //renderer.draw(text2);
                //renderer.draw(spriteBckgrnd);
                renderer.setView(mainView);
                gameEntities.render(renderer);
                renderer.setView(hudView);
                hudEntities.render(renderer);
                renderer.display();
                actions.reset();
                actionsGame.reset();
            }
        }
        return 0;
    }
}
