#ifndef PLATEFORMER2021_MAP_H
#define PLATEFORMER2021_MAP_H

#include <gf/Entity.h>
#include <gf/TileLayer.h>
#include <gf/Texture.h>
#include <gf/Array2D.h>
#include <gf/Map.h>
#include <gf/Random.h>
#include <gf/Message.h>
#include <gf/Rect.h>
#include <gf/VertexArray.h>
#include <gf/Tmx.h>
#include <gf/Sprite.h>

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "Singletons.h"
#include "Messages.h"

#include "Player.h"

#include "box2d/box2d.h"
#include "Victoire.h"

namespace plateformer2021 {

    class MapScene : public gf::Entity {
      public:

        //attributs

        //int currentMap;
        bool isOnEndingPoint;
        bool finished;

        //static constexpr int TilesetTileSize = 256;
        static constexpr int TileSize = 16;//carr�s 16x16

        enum class Tile {
            Ground, GroundWithItem
        };

        gf::Vector2f endFlagPos;

        //coordonn�es x y du joueur
        gf::Vector2f playerCoords;
        Player* playerP;
        Player player;

        b2World* worldP;
        b2Vec2 gravity;
        b2Body* playerBody;

        //sprites blocs
        const int numberOfSprites;
        //std::vector<gf::Sprite> allSprites;
        //gf::Sprite allSprites[120];

        //gf::Sprite movableSprites[100];
        /*std::vector<gf::Sprite> movableSprites;

        std::vector<gf::Sprite> staticSprites;*/

        std::vector<b2Body*> staticBodiesFromPhysics;
        std::vector<b2Body*> wallBodiesFromPhysics;

        std::vector<b2Body*> movingBodiesFromPhysics;
        std::vector<b2Body*> elevatorBodiesFromPhysics;
        std::vector<b2Body*> pushableBodiesFromPhysics;

        std::vector<b2Body*> fallingBodiesFromPhysics;
        std::vector<b2Vec2> fallingBodiesInitialPosFromPhysics;
        std::vector<bool> cling;

        std::vector<b2Body*> climbedBodiesFromPhysics;

        std::vector<b2Body*> ennemiesBodiesFromPhysics;

        std::vector<b2Body*> trapBodiesFromPhysics;

        //messages
        gf::MessageStatus onNewMap(gf::Id id, gf::Message* msg);
        gf::MessageStatus onResetMap(gf::Id id, gf::Message* msg);
        gf::MessageStatus onMapLost(gf::Id id, gf::Message* msg);
        gf::MessageStatus onPlayerPosition(gf::Id id, gf::Message* msg);
        gf::MessageStatus onEndingPoint(gf::Id id, gf::Message* msg);

        //constructeur

        MapScene(Player pl);

        MapScene();

        //methodes

        virtual void update(gf::Time time) override;

        virtual void render(gf::RenderTarget& target, const gf::RenderStates& states) override;

        void drawMap(gf::RenderTarget& target, const gf::RenderStates& states);

        gf::Sprite* CreateMapBody();

        void clearMap();

        void generateWorld(gf::Random& random);

        bool isMapFinished() const {
            return finished;
        }

        void drawEndFlag(gf::RenderTarget& target, const gf::RenderStates& states);

        /*void setBlocPosition(int i,gf::Vector2f position) {
            movableSprites[i].setPosition(position);
        }

        gf::Vector2f getBlocPosition(int i) const {
            return movableSprites[i].getPosition();
        }*/

    };
}

#endif