#ifndef PLATEFORMER2021_PLAYER_H
#define PLATEFORMER2021_PLAYER_H

#include <gf/Animation.h>
#include <gf/Direction.h>
#include <gf/Entity.h>
#include <gf/Message.h>
#include <gf/Vector.h>

#include <box2d/box2d.h>

#include "Messages.h"

#include "Defaite.h"

namespace plateformer2021 {

    class Player : public gf::Entity {

    public:

        Player();

        gf::MessageStatus onNewMap(gf::Id id, gf::Message* msg);
        gf::MessageStatus onResetMap(gf::Id id, gf::Message* msg);

        gf::Vector2f initialPosition;
        gf::Vector2f position;
        gf::Vector2f prevPosition;
        gf::Vector2f velocity;

        gf::Vector2f positionBeforeDeath;

        gf::Vector2f endFlagPos;

        bool win;

        bool isOnAir;

        gf::Direction direction;//East North West South NorthEast NorthWest SouthEast SouthWest
        bool isRunning;
        bool isJumping;
        bool isFalling;
        bool isClimbingTop;
        bool isClimbingDown;

        bool isOnLadder;

        bool isJumpingPhysics;//utiliser isJumping dans le update de Physics priorise celui du update de Player, on cr�e donc une autre variable d�di�e au saut physique
        //isJumping sera utilis� pour l'animation seulement, pas le changement de position (pour g�rer collisions avec capteurs)

        int countJump;

        float lowestground;

        int lives;

        //physique
        b2Body* playerBody;//le corps du joueur
        b2Body* lastGround;//le dernier sol sur lequel il a �t�

        gf::Animation nomove[2];//orient� � gauche ou � droite
        gf::Animation running[4];//orient� � gauche ou � droite (2frames pour chaque c�t�)
        gf::Animation jumping[2];//orient� � gauche ou � droite
        gf::Animation* currentAnimation;
        gf::Animation death;
        gf::Animation climbing[2];
        gf::Animation falling;
        gf::Animation victory;
        
        void goRight() {
            isRunning = true;
            direction = gf::Direction::Right;
            //isJumping = false;
            //isClimbingTop = false;
            //isClimbingDown = false;
        }

        void goLeft() {
            isRunning = true;
            direction = gf::Direction::Left;
            //isJumping = false;
            //isClimbingTop = false;
            //isClimbingDown = false;            
        }

        //pour le saut on doit prendre en compte le "poids" du joueur
        void Jump() {
            //isRunning = false;
            isJumping = true;
            isJumpingPhysics = true;
            isClimbingTop = false;
            isClimbingDown = false;
        }

        void ClimbUp() {
            isClimbingTop = true;
            isClimbingDown = false;
            if (!isOnLadder) {
                isRunning = false;
            }
        }

        void ClimbDown() {
            isClimbingDown = true;
            isClimbingTop = false;
            if (!isOnLadder) {
                isRunning = false;
            }
        }

        void stop() {
            isRunning = false;
            isJumping = false;
            isJumpingPhysics = false;
            isClimbingTop = false;
            isClimbingDown = false;
        }

        struct Dynamics {
            gf::Vector2f position;
            gf::Vector2f velocity;
        };

        void setDynamics(const Dynamics& dynamics) {
            position = dynamics.position;
            velocity = dynamics.velocity;
        }

        Dynamics getDynamics() const {
            return { position, velocity };
        }

        void setPlayerBodyFromPhysics(b2Body* bodyOfPl);

        virtual void update(gf::Time time) override;

        virtual void render(gf::RenderTarget& target, const gf::RenderStates& states) override;

    };

}
#endif