#ifndef PLATEFORMER2021_SINGLETONS_H
#define PLATEFORMER2021_SINGLETONS_H

#include <gf/Singleton.h>
#include <gf/MessageManager.h>
#include <gf/ResourceManager.h>
#include "ResourceManager.h"

namespace plateformer2021 {
	extern gf::Singleton<gf::MessageManager> msgMnger;
	extern gf::Singleton<ResourceManager> rsc;
}

#endif