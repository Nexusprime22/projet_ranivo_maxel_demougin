#include "Victoire.h"

#include <gf/Coordinates.h>
#include <gf/RenderTarget.h>
#include <gf/Sprite.h>
#include <gf/VectorOps.h>

#include "Singletons.h"

namespace plateformer2021 {

    Victoire::Victoire()
  : gf::Entity(100000)
  , m_texture(rsc().getTexture(aleaScreenVictoire())) // Retourne le chemin d'une image de victoire aléatoirement
  {
  }

    std::string Victoire::aleaScreenVictoire(){
    int ind = rand()%2;
    switch (ind){
       case 0:
          return "texture/victoire/victoire.png";
       case 1:
          return "texture/victoire/victoire2.png";
    }
  }

  void Victoire::setPositionFin(float x, float y) {
      positionFin = { x,y };
  }


  void Victoire::render(gf::RenderTarget& target, const gf::RenderStates& states) {

      gf::Sprite victoire;
      victoire.setPosition({ positionFin.x-55,positionFin.y-30});//sur première map (430,190)
      victoire.setScale({ 0.25 });
      victoire.setTexture(m_texture);
      target.draw(victoire, states);
  }

}
