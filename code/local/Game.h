#ifndef PLATEFORMER2021_GAME_H
#define PLATEFORMER2021_GAME_H

#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/Entity.h>

#include <iostream>
#include <cstdlib>
#include <cassert>

#include <box2d/box2d.h>

namespace plateformer2021 {

    class Game : public gf::Entity {

    public:
        int tutoState = 0;
        Game();
        void launchGame(gf::Window &window,Menu mainmenu,float scaleX,float scaleY);
    };
}
#endif