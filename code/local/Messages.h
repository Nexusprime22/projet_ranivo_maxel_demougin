#ifndef PLATEFORMER2021_MESSAGES_H
#define PLATEFORMER2021_MESSAGES_H

#include <gf/Message.h>
#include <gf/Rect.h>
#include <gf/Vector.h>

#include <box2d/box2d.h>

using namespace gf::literals;

namespace plateformer2021 {

    struct PlayerPositionMessage : public gf::Message {

    public:

        static constexpr gf::Id type = "PlayerPos"_id;

        gf::Vector2f position;
        gf::RectF bounds;
        bool isRunning;
        bool isJumping;
    };

    struct OnPlayerPositionMessage : public gf::Message {
        static constexpr gf::Id type = "OnPlayerPos"_id;

        gf::Vector2f position;
        gf::RectF bounds;
    };

    struct OnEndingPointMessage : public gf::Message {
        static constexpr gf::Id type = "Ending"_id;
    };

    /*
     * messages li�s � la map sur laquelle se trouve le joueur
     */

    struct NextMapMessage : public gf::Message {
        static constexpr gf::Id type = "NewMap"_id;

        gf::Vector2f playerPosition;
    };

    struct ResetMapMessage : public gf::Message {
        static constexpr gf::Id type = "ResetMap"_id;
    };

    /*
     * messages li�s � des objets, obstacles crois�s/en contact avec le joueur
     */

    struct BlockedBy_Message : public gf::Message {
        static constexpr gf::Id type = "BlockedBy"_id;
        unsigned number;
    };

    struct InWaterMessage : public gf::Message {
        static constexpr gf::Id type = "InWater"_id;
        unsigned number;
    };

    struct OnBlockMessage : public gf::Message {
        static constexpr gf::Id type = "OnBlock"_id;
        gf::Vector2f position;
    };

    //game over
    struct LostMessage : public gf::Message {
        static constexpr gf::Id type = "Fail"_id;
    };

    struct Victory : public gf::Message {
        static const gf::Id type = "Victory"_id;
    };

}

#endif
