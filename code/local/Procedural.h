#ifndef PLATEFORMER2021_PROCEDURAL_H
#define PLATEFORMER2021_PROCEDURAL_H


#include <gf/Entity.h>
#include <gf/Texture.h>
#include <gf/Model.h>

namespace plateformer2021 {

    struct room {
        bool alreadyPassed;

        // Position cardinnal  la matrice de la structure avant et apres 
        char positionBefore;
        char positionAfter;

        //Position du point 
        int checkpointX;
        int checkpointY;
        //Structure qui sont acoll�
        struct room* before;
        struct room* after;

        std::vector<char> matrice;
    };

    struct allPosition {
        int nord;
        int sud;
        int est;
        int ouest;
    };

  class Procedural : public gf::Model {
  public:

      //attributs
      std::vector<struct room> map;

      size_t start_room;
      size_t end_room;

      // Matrice principale
      int hauteurMatrice = 5;
      int largeurMatrice = 6;


      // Sous matrice
      int hauteurSousMatrice = 16;
      int largeurSousMatrice = 20;

      //constructeur
    Procedural();

    int alea(int posibility);

    void playerPosition(std::vector<struct room>& path);

    void build(std::vector<struct room>& path, int start);
    void secondBuild(std::vector<struct room>& path);
    void thirtyBuild(std::vector<struct room>& path);
    void fourtyBuild(std::vector<struct room>& path);

    void createLiana(std::vector<struct room>& path);
    bool completeLiana(std::vector<char>& matrice, int start);

    void recessLadderBuild(std::vector<char>& matrice, int start);

    void recessGroundBuild(std::vector<char>& matrice, int start);

    void recessBuild(std::vector<struct room>& path);

    void mobilePlatformBuild(std::vector<struct room>& path);

    bool controlPlacementEnnemy(std::vector<char>& matrice, int start, int mobile);

    void introduceEnnemy(std::vector<char>& matrice, int start);

    void introduceTrap(std::vector<char>& matrice, int start);

    void createEnnemyTrapMortal(std::vector<struct room>& path);

    void hiddenPath(std::vector<struct room>& path);
    void buildWall(std::vector<struct room>& path);

    void primary();

    void imp(std::vector<struct room>& path);

    void clearMap(std::vector<struct room>& path);
    void startPlayer(struct room start);

  };

}

#endif // PLATEFORMER2021_PROCEDURAL_H
