//surcharge pour permettre d'aller plus loin que le getTexture possible avec rsc(), on ajoute getSound() et getMusic()

#ifndef PLATEFORMER2021_RESOURCEMANAGER_H
#define PLATEFORMER2021_RESOURCEMANAGER_H

#include <gf/ResourceManager.h>
#include <SFML/Audio.hpp>

namespace plateformer2021 {

    class ResourceManager : public gf::ResourceManager {
    public:
        ResourceManager();
        sf::SoundBuffer& getSound(const gf::Path& path);
        sf::Music& getMusic(const gf::Path& path);

    private:
        gf::ResourceCache<sf::SoundBuffer> sounds;
        gf::ResourceCache<sf::Music> musics;
    };
}

#endif // PLATEFORMER2021_RESOURCEMANAGER_H
