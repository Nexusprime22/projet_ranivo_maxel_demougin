/*

    MAXEL Killian TP1A
    DEMOUGIN Alexandre TP1A
    RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/Unused.h>

#include "Menu.h"
#include "Physics.h"

#include "../../build/config.h"

#include <gf/Random.h>
#include <gf/Singleton.h>

#include "MapScene.h"
#include "Singletons.h"
#include "Messages.h"
#include "Player.h"
#include "PhysicsDebugger.h"//pour avoir un affichage des objets physiques
#include "Music.h"

#include <iostream>
#include <cstdlib>
#include <cassert>
#include <box2d/box2d.h>
#include <gf/ModelContainer.h>
#include <gf/Log.h>

#include <SFML/Audio.hpp>
#include "Game.h"

namespace plateformer2021 {

    static constexpr float SoundsVolume = 5.0f;

    Player reinitPlayer(Player pl) {
        //pour empecher que le perso soit toujours consid�r� au sol de victoire m�me � pos diff�rente
        if (pl.win) {
            pl.playerBody->SetLinearVelocity({ 0.0f,0.01f });
            pl.setPlayerBodyFromPhysics(pl.playerBody);
        }

        if (pl.isFalling || pl.win == false) {
            pl.playerBody = nullptr;
        }
        pl.position = pl.initialPosition;
        pl.isFalling = false;
        pl.direction = gf::Direction::Right;
        pl.isRunning = false;
        pl.isJumping = false;
        pl.isClimbingTop = false;
        pl.isClimbingDown = false;
        pl.countJump = 0;
        pl.win = false;
        pl.currentAnimation = &pl.nomove[0];
        pl.positionBeforeDeath = pl.initialPosition;
        return pl;
    }

    void supprVectorsFromPhysics(Physics phys, MapScene map) {
        printf("number of bodies in box2dWorld before erasing : %i\n", phys.world->GetBodyCount());
        printf("number of elements in staticbodies before clear : %zu\n", phys.staticBodies.size());
        printf("number of elements in staticbodies in map before clear : %zu\n", map.staticBodiesFromPhysics.size());
        printf("capacity in staticbodies before clear : %zu\n", phys.staticBodies.capacity());
        printf("capacity in staticbodies in map before clear : %zu\n\n", map.staticBodiesFromPhysics.capacity());

        phys.climbedBodies.clear();
        phys.movingBodies.clear();
        phys.elevatorBodies.clear();
        phys.fallingBodies.clear();
        phys.pushableBodies.clear();
        phys.staticBodies.clear();
        phys.trapBodies.clear();
        phys.ennemiesBodies.clear();
        phys.wallBodies.clear();

        map.climbedBodiesFromPhysics.clear();
        map.fallingBodiesFromPhysics.clear();
        map.cling.clear();
        map.fallingBodiesInitialPosFromPhysics.clear();
        map.movingBodiesFromPhysics.clear();
        map.elevatorBodiesFromPhysics.clear();
        map.pushableBodiesFromPhysics.clear();
        map.staticBodiesFromPhysics.clear();
        map.trapBodiesFromPhysics.clear();
        map.ennemiesBodiesFromPhysics.clear();
        map.wallBodiesFromPhysics.clear();

        phys.pushableBodiesInitialPos.clear();
        phys.fallingBodiesInitialPos.clear();
        phys.ennemiesBodiesInitialPos.clear();

        b2Body* body = phys.world->GetBodyList();
        while (body) {
            b2Body* b = body;
            body = body->GetNext();
            phys.world->DestroyBody(b);
        }

        printf("number of bodies in box2dWorld after erasing : %i\n", phys.world->GetBodyCount());
        printf("number of elements in staticbodies in phys after clear : %zu\n", phys.staticBodies.size());
        printf("number of elements in staticbodies in map after clear : %zu\n", map.staticBodiesFromPhysics.size());
        printf("capacity in staticbodies after clear : %zu\n", phys.staticBodies.capacity());
        printf("capacity in staticbodies in map after clear : %zu\n\n", map.staticBodiesFromPhysics.capacity());

        phys.world = nullptr;
        b2World newWorld({ 0.0,60.0f });
        phys.world = &newWorld;

    }

    Game::Game() {
    }

    static constexpr gf::Vector2u ScreenSize(1024, 576);
    static constexpr gf::Vector2i ViewSize(200.0f, 200.0f);
    static constexpr gf::Vector2i ViewCenter(0.0f, 0.0f);

    void Game::launchGame(gf::Window &window, Menu mainmenu,float scaleX,float scaleY) {

        printf("NEW GAME \n");

        rsc().addSearchDir(PLATEFORMER2021_DATA_DIR);

        // initialization
        //gf::Window window("Plateformer2021", ScreenSize);
        window.setVerticalSyncEnabled(true);
        window.setFramerateLimit(60);
        gf::RenderWindow renderer(window);

        // cursor

        // views
        gf::ViewContainer views;
        gf::ExtendView mainView(ViewCenter, ViewSize);
        views.addView(mainView);
        gf::ScreenView hudView;
        views.addView(hudView);
        views.setInitialFramebufferSize(ScreenSize);

        //touches du jeu

        gf::ActionContainer actionsGame;

        gf::Action closeWindowAction("Close window");
        closeWindowAction.addCloseControl();
        closeWindowAction.addKeycodeKeyControl(gf::Keycode::Escape);
        actionsGame.addAction(closeWindowAction);

        gf::Action fullscreenAction("Fullscreen");
        fullscreenAction.addKeycodeKeyControl(gf::Keycode::F);
        actionsGame.addAction(fullscreenAction);

        gf::Action muteMusicAction("Mute");
        muteMusicAction.addScancodeKeyControl(gf::Scancode::V);
        muteMusicAction.addScancodeKeyControl(gf::Scancode::F1);
        muteMusicAction.setInstantaneous();
        actionsGame.addAction(muteMusicAction);

        gf::Action okAction2("Ok2");
        okAction2.addKeycodeKeyControl(gf::Keycode::Return);
        okAction2.setContinuous();
        actionsGame.addAction(okAction2);

        gf::Action leftAction("Left");
        leftAction.addScancodeKeyControl(gf::Scancode::A);
        leftAction.addScancodeKeyControl(gf::Scancode::Left);
        leftAction.setContinuous();
        actionsGame.addAction(leftAction);

        gf::Action rightAction("Right");
        rightAction.addScancodeKeyControl(gf::Scancode::D);
        rightAction.addScancodeKeyControl(gf::Scancode::Right);
        rightAction.setContinuous();
        actionsGame.addAction(rightAction);

        gf::Action upClimbAction("UpClimb");
        upClimbAction.addScancodeKeyControl(gf::Scancode::Z);
        upClimbAction.addScancodeKeyControl(gf::Scancode::Up);
        upClimbAction.setContinuous();
        actionsGame.addAction(upClimbAction);

        gf::Action downClimbAction("DownClimb");
        downClimbAction.addScancodeKeyControl(gf::Scancode::S);
        downClimbAction.addScancodeKeyControl(gf::Scancode::Down);
        downClimbAction.setContinuous();
        actionsGame.addAction(downClimbAction);

        gf::Action jumpAction("Space");
        jumpAction.addScancodeKeyControl(gf::Scancode::W);
        jumpAction.addScancodeKeyControl(gf::Scancode::Space);
        jumpAction.setContinuous();
        actionsGame.addAction(jumpAction);

        gf::Action printPhysics("PhysicsDebugger");
        printPhysics.addScancodeKeyControl(gf::Scancode::P);
        printPhysics.setInstantaneous();
        actionsGame.addAction(printPhysics);

        // entities
        gf::EntityContainer mainEntities;

        // add entities to mainEntities
        gf::EntityContainer hudEntities;
        // add entities to hudEntities

        // game loop
        renderer.clear(gf::Color::Cyan);
        gf::Clock clock;

        //ELEMENTS DU JEU

        gf::EntityContainer gameEntities;

        /*float scaleX = (float)renderer.getSize().width / 1024;
        float scaleY = (float)renderer.getSize().height / 576;*/

        plateformer2021::Player pl;

        plateformer2021::msgMnger().registerHandler<plateformer2021::PlayerPositionMessage>([&mainView](gf::Id id, gf::Message* msg) {
            assert(id == plateformer2021::PlayerPositionMessage::type);
            gf::unused(id);
            auto plPosition = static_cast<plateformer2021::PlayerPositionMessage*>(msg);
            mainView.setCenter(plPosition->position);
            return gf::MessageStatus::Keep;
            });

        //affichage de map et physique

        plateformer2021::MapScene mp(pl);

        gameEntities.addEntity(mp);
        gameEntities.addEntity(pl);

        gf::ModelContainer models;
        Physics physics(pl, mp);
        models.addModel(physics);

        //physics debugger

        plateformer2021::PhysicsDebugger debug(physics);
        gameEntities.addEntity(debug);
        bool boolDebug = true;

        window.setMouseCursorVisible(false);

        //sons

        plateformer2021::Music music;//� placer � la fin de tous les autres �l�ments sinon crash
        music.gameMusic();

        sf::Sound jumpSound(rsc().getSound("sounds/jump.ogg"));
        sf::Sound deathSound(rsc().getSound("sounds/death.ogg"));
        sf::Sound yeahSound(rsc().getSound("sounds/yeah.ogg"));
        sf::Sound victorySound(rsc().getSound("sounds/victory.ogg"));
        sf::Sound defeatSound(rsc().getSound("sounds/defeat.ogg"));
        sf::Sound hurtSound(rsc().getSound("sounds/hurt.ogg"));
        sf::Sound pickSound(rsc().getSound("sounds/pickup.ogg"));
        sf::Sound pushSound(rsc().getSound("sounds/push.ogg"));

        sf::Sound selectSound(rsc().getSound("sounds/select.ogg"));
        sf::Sound okSound(rsc().getSound("sounds/ok.ogg"));

        jumpSound.setVolume(SoundsVolume);
        deathSound.setVolume(SoundsVolume);
        yeahSound.setVolume(SoundsVolume);
        victorySound.setVolume(SoundsVolume);
        defeatSound.setVolume(SoundsVolume);
        hurtSound.setVolume(SoundsVolume);
        pickSound.setVolume(SoundsVolume);
        pushSound.setVolume(SoundsVolume);
        selectSound.setVolume(SoundsVolume);
        okSound.setVolume(SoundsVolume);

        int ntime = 0;

        int nlives = pl.lives;

        bool returnToMenu = false;

        supprVectorsFromPhysics(physics, mp);

        if (tutoState == 0) {
            nlives = 3;

            printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

            pl = reinitPlayer(pl);
            physics.generatePhysicalWorld();
            pl = reinitPlayer(pl);
            physics.createPhysicalPlayer();

            printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

            static constexpr gf::Vector2i ProceduralViewSize(200.0f, 200.0f);
            mainView.setSize(ProceduralViewSize);

        }
        else {

            nlives = 3;

            printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

            pl = reinitPlayer(pl);
            physics.generateTutorialPhysicalWorld();
            pl = reinitPlayer(pl);
            physics.createPhysicalPlayer();

            printf("(%f,%f) (%f,%f)\n", pl.position.x, pl.position.y, pl.initialPosition.x, pl.initialPosition.y);

            static constexpr gf::Vector2i ProceduralViewSize(200.0f, 200.0f);
            mainView.setSize(ProceduralViewSize);

            /*pl = reinitPlayer(pl);
            physics.player = pl;
            nlives = 3;

            physics.createPhysicalPlayer();
            physics.generateTutorialPhysicalWorld();

            static constexpr gf::Vector2i TutorialViewSize(200.0f, 200.0f);
            mainView.setSize(TutorialViewSize);*/

        }

        while (window.isOpen()) {
            scaleX = (float)renderer.getSize().width / 1024;
            scaleY = (float)renderer.getSize().height / 576;
           // printf("scaleX : %f\n",scaleX);

            gf::Event event;

            while (window.pollEvent(event)) {

                actionsGame.processEvent(event);
                views.processEvent(event);
            }

            if (muteMusicAction.isActive()) {
                music.mute();
                printf("desactivation de la musique\n");
            }
            if (pl.isFalling && ntime == 0) {
                ntime = 1;
                deathSound.play();
            }

            if (pl.isFalling && okAction2.isActive()) {
                nlives--;
                pl = reinitPlayer(pl);
                physics.player = pl;
                pl.lives = nlives;
                physics.createPhysicalPlayer();
                //map rinitialise ou retour au menu ?
                printf("nombre de vies restantes : %i\n", pl.lives);
                if (pl.lives == 0) {
                    supprVectorsFromPhysics(physics, mp);
                    reinitPlayer(pl);
                    nlives = 3;
                    returnToMenu = true;
                    break;
                    //window.close();
                }
            }
            if (pl.win && ntime == 0) {
                yeahSound.play();
                ntime = 1;
            }
            if (pl.win && okAction2.isActive()) {
                pl = reinitPlayer(pl);
                pl.lives = nlives;
                physics.player = pl;
                mp.player = pl;
                mp.playerP = &pl;
                okSound.play();
                supprVectorsFromPhysics(physics, mp);
                ntime = 0;
                //map suivante
                physics.generatePhysicalWorld();
                pl = reinitPlayer(pl);
                physics.createPhysicalPlayer();
            }

            if (printPhysics.isActive()) {
                //printf("Print physics\n");
                if (boolDebug) {
                    boolDebug = false;
                }
                else {
                    boolDebug = true;
                }
                debug.setDebug(boolDebug);
            }

            if (!pl.win && !pl.isFalling) {
                if (upClimbAction.isActive() && jumpAction.isActive() && leftAction.isActive()) {
                    pl.Jump();
                    pl.goLeft();
                    pl.ClimbUp();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (downClimbAction.isActive() && jumpAction.isActive() && leftAction.isActive()) {
                    pl.Jump();
                    pl.goLeft();
                    pl.ClimbDown();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (upClimbAction.isActive() && jumpAction.isActive() && rightAction.isActive()) {
                    pl.Jump();
                    pl.goRight();
                    pl.ClimbUp();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (downClimbAction.isActive() && jumpAction.isActive() && rightAction.isActive()) {
                    pl.Jump();
                    pl.goRight();
                    pl.ClimbDown();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (rightAction.isActive() && upClimbAction.isActive()) {
                    pl.goRight();
                    pl.ClimbUp();
                }
                else if (rightAction.isActive() && downClimbAction.isActive()) {
                    pl.goRight();
                    pl.ClimbDown();
                }
                else if (leftAction.isActive() && upClimbAction.isActive()) {
                    pl.goLeft();
                    pl.ClimbUp();
                }
                else if (leftAction.isActive() && downClimbAction.isActive()) {
                    pl.goLeft();
                    pl.ClimbDown();
                }
                else if (rightAction.isActive() && jumpAction.isActive()) {
                    pl.Jump();
                    pl.goRight();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (upClimbAction.isActive() && jumpAction.isActive()) {
                    pl.Jump();
                    pl.ClimbUp();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (downClimbAction.isActive() && jumpAction.isActive()) {
                    pl.Jump();
                    pl.ClimbDown();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (leftAction.isActive() && jumpAction.isActive()) {
                    pl.Jump();
                    pl.goLeft();
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (leftAction.isActive()) {
                    pl.goLeft();
                }
                else if (rightAction.isActive()) {
                    pl.goRight();
                }
                else if (jumpAction.isActive()) {
                    pl.Jump();
                    //physics.bodyJump();
                    //printf("(%f,%f)\n", pl.playerBody->GetPosition().x, pl.playerBody->GetPosition().y);
                    //printf("(%f,%f)\n", pl.position.x, pl.position.y);
                    if (jumpSound.getStatus() != sf::SoundSource::Playing) {
                        jumpSound.play();
                    }
                }
                else if (upClimbAction.isActive()/*&& persoSurEchelle*/) {
                    pl.ClimbUp();
                    //monte
                    physics.player = pl;
                    mp.player = pl;
                    mp.playerP = &pl;
                }
                else if (downClimbAction.isActive()) {
                    //printf("Climbing down before ? %d", pl.isClimbingDown);
                    pl.ClimbDown();
                    //descend
                    //printf("Climbing down after ? %d", pl.isClimbingDown);
                    physics.player = pl;
                    mp.player = pl;
                    mp.playerP = &pl;
                }
                else {
                    pl.stop();
                }
            }


            if (closeWindowAction.isActive()) {
                supprVectorsFromPhysics(physics, mp);
                //music.mute();
                returnToMenu = true;
                break;
                //window.close();
                /*plateformer2021::Menu mainMenu;
                mainMenu.printMenu(window);*/
            }
            if (fullscreenAction.isActive()) {
                window.toggleFullscreen();
                if (!window.isFullscreen()) {
                    scaleX = 1;
                    scaleY = 1;
                }
                else {
                    scaleX = 2;
                    scaleY = 2;
                }
            }
            /*if (muteMusicAction.isActive()) {
                printf("desactivation de la musique\n");
                music.mute();
            }*/

            // 2. update
            gf::Time time = clock.restart();
            gameEntities.update(time);
            hudEntities.update(time);
            models.update(time);

            // 3. draw
            renderer.clear();
            //renderer.draw(text2);
            //renderer.draw(spriteBckgrnd);
            renderer.setView(mainView);
            gameEntities.render(renderer);
            renderer.setView(hudView);
            hudEntities.render(renderer);
            renderer.display();
            actionsGame.reset();

        }

        if (returnToMenu) {
            supprVectorsFromPhysics(physics, mp);
            //window.~Window();
            music.mute();
            mainmenu.printMenu(window,scaleX,scaleY);
        }
    }
}
