#include "Defaite.h"

#include <gf/Coordinates.h>
#include <gf/RenderTarget.h>
#include <gf/Sprite.h>
#include <gf/VectorOps.h>

#include "Singletons.h"

int ind = 0;

namespace plateformer2021 {

    Defaite::Defaite()
  : m_texture(rsc().getTexture(aleaScreenDefaite())) // Retourne le chemin d'une image de defaite aléatoirement
  , m_textTexture(rsc().getTexture(aleaTextDefaite()))
  {
  }

  std::string Defaite::aleaScreenDefaite(){
    switch (ind){
      case 0:
        ind++;
        return "texture/defaite/death_2.png";
      case 1:
        ind++;
        return "texture/defaite/death_2.png";
      case 2:
          ind++;
        return "texture/defaite/death_2.png";
      case 3:
          ind++;
        return "texture/defaite/death_2.png";
      case 4:
          ind++;
        return "texture/defaite/death_3.png";
      case 5:
          ind++;
          return "texture/defaite/death_3.png";
      case 6:
          ind++;
          return "texture/defaite/death_3.png";
      case 7:
          ind = 0;
          return "texture/defaite/death_3.png";
    }
  }

  std::string Defaite::aleaTextDefaite() {
      switch (ind) {
      case 1:
          return "texture/defaite/defaite1.png";
      case 2:
          return "texture/defaite/defaite1.png";
      case 3:
          return "texture/defaite/defaite1.png";
      case 4:
          return "texture/defaite/defaite1.png";
      case 5:
          return "texture/defaite/defaite2.png";
      case 6:
          return "texture/defaite/defaite2.png";
      case 7:
          return "texture/defaite/defaite2.png";
      case 0:
          return "texture/defaite/defaite2.png";
      }
  }


  void Defaite::setPositionMort(float x, float y) {
      positionMort = { x,y };
  }


  void Defaite::render(gf::RenderTarget& target, const gf::RenderStates& states) {

    gf::Sprite defaite;
    defaite.setPosition({ positionMort.x - 35,positionMort.y - 20 });//sur première map (430,190)
    defaite.setScale({ 0.25 });
    defaite.setTexture(m_texture);
    target.draw(defaite, states);

    gf::Sprite defeatText;
    defeatText.setTexture(m_textTexture);
    defeatText.setPosition({ positionMort.x - 50,positionMort.y - 30 });
    defeatText.setScale({ 0.25 });
    target.draw(defeatText, states);

  }

}
