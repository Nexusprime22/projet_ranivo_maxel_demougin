/*

    MAXEL Killian TP1A
    DEMOUGIN Alexandre TP1A
    RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/Unused.h>

#include "Menu.h"
#include "Physics.h"

#include "../../build/config.h"

//#include <gf/ResourceManager.h>
//#include <gf/MessageManager.h>
#include <gf/Random.h>
#include <gf/Singleton.h>

#include "MapScene.h"
#include "Singletons.h"
#include "Messages.h"
#include "Player.h"
#include "PhysicsDebugger.h"//pour avoir un affichage des objets physiques
#include "Music.h"

#include <iostream>
#include <cstdlib>
#include <cassert>
#include <box2d/box2d.h>
#include <gf/ModelContainer.h>
#include <gf/Log.h>

#include <SFML/Audio.hpp>

#include "Game.h"

namespace plateformer2021{

    static constexpr float SoundsVolume = 5.0f;

    Menu::Menu()
    {
    }
    
    int Menu::initMenu(gf::Window &window,float scaleX,float scaleY){
      if (plateformer2021::msgMnger.isValid()) {
	  printf("msgMnger already Initialized\n");
	  printMenu(window,scaleX,scaleY);
      }
      else {
            gf::SingletonStorage<gf::MessageManager> storageForMessageManager(plateformer2021::msgMnger);
            
            if (plateformer2021::rsc.isValid()) {
	            printf("rsc already Initialized\n");
	            printMenu(window,scaleX,scaleY);
      	    }
            else {
                gf::SingletonStorage<ResourceManager> storageForResourceManager(plateformer2021::rsc);
                printMenu(window,scaleX,scaleY);
            }     
            
      }
      return 0;
          
    }

    /*float scaleX = (float)renderer.getSize().width / 1024;
    float scaleY = (float)renderer.getSize().height / 576;*/

    static constexpr gf::Vector2u ScreenSize(1024, 576);
    static constexpr gf::Vector2i ViewSize(200.0f, 200.0f);
    static constexpr gf::Vector2i ViewCenter(0.0f, 0.0f);

    int Menu::printMenu(gf::Window &window, float scaleX, float scaleY) {

        rsc().addSearchDir(PLATEFORMER2021_DATA_DIR);

        // initialization
        //gf::Window window("Plateformer2021", ScreenSize);
        window.setVerticalSyncEnabled(true);
        window.setFramerateLimit(60);
        gf::RenderWindow renderer(window);

        // Load an image file from a file

        gf::Path pathmenu = "texture/menu.png";

        gf::Texture& texture= rsc().getTexture(pathmenu);
        gf::Sprite sprite;
        sprite.setTexture(texture);
        sprite.setScale({ scaleX,scaleY });
        //printf("sprite scale %f\n", sprite.getScale().x);

        gf::Path pathchoice = "texture/choice.png";

        gf::Texture& texture2= rsc().getTexture(pathchoice);
        gf::Sprite sprite2;
        sprite2.setTexture(texture2);
        //sprite2.setPosition({ 550,410 });//gauche-droite/haut-bas
        sprite2.setPosition({ 550 * scaleX,410 * scaleY });

        // Load text

        gf::Path pathFont = "texture/impact.ttf";
        gf::Font& font=rsc().getFont(pathFont);
        gf::Text text("Play", font, 50);
        //text.setPosition({ 465,450 });
        text.setPosition({ 465 * scaleX, 450 * scaleY });

        gf::Text text2("Tuto", font, 50);
        text2.setPosition({ 465 * scaleX, 500 * scaleY });

        gf::Text text3("Leave", font, 50);
        text3.setPosition({ 465 * scaleX, 550 * scaleY });

        // cursor

        // views
        gf::ViewContainer views;
        gf::ExtendView mainView(ViewCenter, ViewSize);
        views.addView(mainView);
        gf::ScreenView hudView;
        views.addView(hudView);
        views.setInitialFramebufferSize(ScreenSize);

        // actions
        gf::ActionContainer actions;

        gf::Action closeWindowAction("Close window");
        closeWindowAction.addCloseControl();
        closeWindowAction.addKeycodeKeyControl(gf::Keycode::Escape);
        actions.addAction(closeWindowAction);

        gf::Action fullscreenAction("Fullscreen");
        fullscreenAction.addKeycodeKeyControl(gf::Keycode::F);
        actions.addAction(fullscreenAction);

        gf::Action okAction("Ok");
        okAction.addKeycodeKeyControl(gf::Keycode::Return);
        okAction.setInstantaneous();
        actions.addAction(okAction);

        gf::Action upAction("Up");
        upAction.addScancodeKeyControl(gf::Scancode::W);
        upAction.addScancodeKeyControl(gf::Scancode::Up);
        upAction.setInstantaneous();
        actions.addAction(upAction);

        gf::Action downAction("Down");
        downAction.addScancodeKeyControl(gf::Scancode::S);
        downAction.addScancodeKeyControl(gf::Scancode::Down);
        downAction.setInstantaneous();
        actions.addAction(downAction);

        gf::Action muteMusicAction("Mute");
        muteMusicAction.addScancodeKeyControl(gf::Scancode::V);
        muteMusicAction.addScancodeKeyControl(gf::Scancode::F1);
        muteMusicAction.setInstantaneous();
        actions.addAction(muteMusicAction);

        // entities
        gf::EntityContainer mainEntities;

        // add entities to mainEntities
        gf::EntityContainer hudEntities;
        // add entities to hudEntities

        // game loop
        renderer.clear(gf::Color::Cyan);
        gf::Clock clock;
        int menu = 1;

        window.setMouseCursorVisible(false);

        plateformer2021::Game game;

        plateformer2021::Music music;//� placer � la fin de tous les autres �l�ments sinon crash

        sf::Sound selectSound(rsc().getSound("sounds/select.ogg"));
        sf::Sound okSound(rsc().getSound("sounds/ok.ogg"));

        selectSound.setVolume(SoundsVolume);
        okSound.setVolume(SoundsVolume);

        bool launched = false;

        while (window.isOpen()) {

            //printf("scaleX apr�s retour menu2 : %f\n", scaleX);

            scaleX = (float)renderer.getSize().width / 1024;
            scaleY = (float)renderer.getSize().height / 576;


            if (menu == 1) {

                // 1. input
                gf::Event event;
                while (window.pollEvent(event)) {
                    actions.processEvent(event);
                    views.processEvent(event);
                }
                if (closeWindowAction.isActive()) {
                    window.close();
                }
                if (fullscreenAction.isActive()) {
                    window.toggleFullscreen();
                    if (!window.isFullscreen()) {
                        scaleX = 1;
                        scaleY = 1;
                    }
                    else {
                        scaleX = 2;
                        scaleY = 2;
                    }
                }

                if (muteMusicAction.isActive()) {
                    printf("desactivation de la musique\n");
                    music.mute();
                }
                if (upAction.isActive() || downAction.isActive()) {
                    selectSound.play();
                }
                if (upAction.isActive() && text.getString()=="Tuto") {
                    text.setString("Play");
                    text2.setString("Tuto");
                    text3.setString("Leave");
                }
                else if (downAction.isActive() && text.getString()=="Play") {
                    text.setString("Tuto");
                    text2.setString("Leave");
                    text3.setString("Play");
                }
                else if (upAction.isActive() && text.getString() == "Leave") {
                    text.setString("Tuto");
                    text2.setString("Leave");
                    text3.setString("Play");
                }
                else if (downAction.isActive() && text.getString() == "Tuto") {
                    text.setString("Leave");
                    text2.setString("Play");
                    text3.setString("Tuto");
                }
                else if (upAction.isActive() && text.getString() == "Play") {
                    text.setString("Leave");
                    text2.setString("Tuto");
                    text3.setString("Play");
                }
                else if (downAction.isActive() && text.getString() == "Leave") {
                    text.setString("Play");
                    text2.setString("Tuto");
                    text3.setString("Leave");
                }
                else if (okAction.isActive() && text.getString() == "Leave") {
                    window.close();
                }
                else if (okAction.isActive() && text.getString() == "Play") {

                    launched = true;
                    game.tutoState = 0;
                    break;
                    //window.close();//on quitte la boucle
                }

                else if (okAction.isActive() && text.getString() == "Tuto" && game.tutoState==0) {

                    game.tutoState = 1;

                }
                else if (okAction.isActive() && text.getString() == "Tuto" && game.tutoState==1) {

                    game.tutoState = 1;
                    launched = true;
                    break;
                    //window.close();//on quitte la boucle
                }
                else {
                    // do something
                }

                // 2. update
                gf::Time time = clock.restart();
                mainEntities.update(time);
                hudEntities.update(time);

                sprite.setScale({ scaleX,scaleY });
                sprite2.setScale({ scaleX,scaleY });
                text.setScale({ scaleX,scaleY });
                text2.setScale({ scaleX,scaleY });
                text3.setScale({ scaleX,scaleY });
                if (text.getString() == "Play") {
                    sprite2.setPosition({ 550 * scaleX,410 * scaleY });
                }
                else {
                    sprite2.setPosition({ 580 * scaleX,410 * scaleY });
                }
                text.setPosition({ 465 * scaleX, 450 * scaleY });
                text2.setPosition({ 465 * scaleX, 500 * scaleY });
                text3.setPosition({ 465 * scaleX, 550 * scaleY });

                // 3. draw
                renderer.clear();
                renderer.draw(sprite);

                renderer.draw(text);
                renderer.draw(text2);
                renderer.draw(text3);
                renderer.draw(sprite2);
                renderer.setView(mainView);
                mainEntities.render(renderer);
                renderer.setView(hudView);
                hudEntities.render(renderer);

                if (game.tutoState == 1) {
                    renderer.clear();
                    gf::Path pathTutoScreen = "texture/tutoScreen.png";
                    gf::Texture& texture = rsc().getTexture(pathTutoScreen);
                    gf::Sprite spriteTuto;
                    spriteTuto.setTexture(texture);
                    spriteTuto.setScale({ scaleX,scaleY });
                    renderer.draw(spriteTuto);
                }

                renderer.display();
                actions.reset();
            }            
        }

        if (launched) {
            //window.~Window();
            music.mute();
            game.launchGame(window,*this, scaleX, scaleY);
        }

        return 0;
    }
}

