#include <vector>

#include <box2d/box2d.h>

#include <gf/Circ.h>
#include <gf/Entity.h>
#include <gf/Polygon.h>
#include <gf/Vector.h>

#include "Physics.h"

namespace plateformer2021 {
  //class Physics;

  class PhysicsDebugger : public gf::Entity {
  public:
    PhysicsDebugger(Physics& model, float opacity = 0.4f);

    void setDebug(bool debug);
    void toggleDebug();

    void render(gf::RenderTarget& target, const gf::RenderStates& states) override;

    struct Polygon {
      gf::Polygon shape;
      gf::Color4f color;
    };

    struct Circle {
      gf::CircF shape;
      gf::Color4f color;
    };

    struct SolidCircle {
      gf::CircF shape;
      gf::Vector2f axis;
      gf::Color4f color;
    };

    struct Segment {
      gf::Vector2f p1;
      gf::Vector2f p2;
      gf::Color4f color;
    };

    struct Transform {
      gf::Vector2f position;
      gf::Vector2f xAxis;
      gf::Vector2f yAxis;
    };

    struct Point {
      gf::Vector2f position;
      float size;
      gf::Color4f color;
    };

    struct PhysicsDraw : public b2Draw {
    public:
      PhysicsDraw(float scale, float opacity);

      /// Draw a closed polygon provided in CCW order.
      void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

      /// Draw a solid closed polygon provided in CCW order.
      void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

      /// Draw a circle.
      void DrawCircle(const b2Vec2& center, float radius, const b2Color& color) override;

      /// Draw a solid circle.
      void DrawSolidCircle(const b2Vec2& center, float radius, const b2Vec2& axis, const b2Color& color) override;

      /// Draw a line segment.
      void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) override;

      /// Draw a transform. Choose your own length scale.
      /// @param xf a transform.
      void DrawTransform(const b2Transform& xf) override;

      /// Draw a point.
      void DrawPoint(const b2Vec2& p, float size, const b2Color& color) override;

      std::vector<Polygon> polygons;
      std::vector<Polygon> solidPolygons;
      std::vector<Circle> circles;
      std::vector<SolidCircle> solidCircles;
      std::vector<Segment> segments;
      std::vector<Transform> transforms;
      std::vector<Point> points;

      gf::Vector2f toGameCoords(b2Vec2 coords);
      gf::Color4f toGameColor(b2Color color);

      float m_scale;
      float m_opacity;
    };

    bool m_debug;
    Physics *m_model;
    PhysicsDraw m_draw;
  };


}