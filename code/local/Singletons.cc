#ifndef PLATEFORMER2021_SINGLETONS_H
#define PLATEFORMER2021_SINGLETONS_H

#include <gf/Singleton.h>
#include <gf/MessageManager.h>
#include <gf/ResourceManager.h>
#include "ResourceManager.h"

#include "Singletons.h"

namespace plateformer2021 {
	gf::Singleton<gf::MessageManager> msgMnger;
	gf::Singleton<ResourceManager> rsc;//on est pass� de gf::ResourceManager � notre classe pour g�rer les musiques
}

#endif