#include <iostream>

#include <vector>
#include <ctime>
#include <fstream>

#include "Procedural.h"

namespace plateformer2021 {

    int numberRoom = 100;
    int totalRoom = 1;

    Procedural::Procedural() {
        primary();
    }


    int Procedural::alea(int posibility) {
        return rand() % posibility;
    }

    /*void Procedural::playerPosition(std::vector<struct room>& map) {
        auto varname = map.begin() + start_room;
        for (size_t i = (4 * largeurSousMatrice); i < largeurSousMatrice * hauteurSousMatrice; ++i) {
            if (varname->matrice[i - largeurSousMatrice] == ' ') {
                if (varname->matrice[i - 2 * largeurSousMatrice] == ' ') {
                    varname->matrice[i - largeurSousMatrice] = 'P';
                    break;
                }
            }
        }
    }*/

    void Procedural::playerPosition(std::vector<struct room>& map) {

        bool position = false, ok = false;
        auto varname = map.begin() + start_room;

        for (size_t i = (4 * largeurSousMatrice); i < largeurSousMatrice * (hauteurSousMatrice - 1); ++i) {

            ok = false;
            for (int j = 0; j < 2; ++j) {

                if ((varname->matrice[i + j] != ' ') || (varname->matrice[i - largeurSousMatrice + j] != ' ') || (varname->matrice[i - 2 * largeurSousMatrice + j] != ' ')) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                varname->matrice[i - largeurSousMatrice + 1] = 'P';
                return;
                break;
            }
        }

        for (size_t i = (4 * largeurSousMatrice); i < largeurSousMatrice * (hauteurSousMatrice - 1); ++i) {
            if ((varname->matrice[i] == ' ') && (varname->matrice[i - largeurSousMatrice] == ' ') && (varname->matrice[i - 2 * largeurSousMatrice] == ' ')) {
                varname->matrice[i] = 'P';
                return;
            }
        }

    }


    void Procedural::build(std::vector<struct room>& path, int start) {

        int goStart = start;
        struct allPosition direction;

        int posibility = 0;

        // Regarde les cases voisines possible de la matrice
        if (start - largeurMatrice >= 0) {
            direction.nord = start - largeurMatrice;
            ++posibility;
        }
        else { direction.nord = -1; }

        if (start + largeurMatrice < (largeurMatrice * hauteurMatrice)) {
            direction.sud = start + largeurMatrice;
            ++posibility;
        }
        else { direction.sud = -1; }

        if (start % largeurMatrice != 0) {
            direction.est = start - 1;
            ++posibility;
        }
        else { direction.est = -1; }

        if (start % largeurMatrice != (largeurMatrice - 1)) {
            direction.ouest = start + 1;
            ++posibility;
        }
        else { direction.ouest = -1; }

        // S'il n'y a plus de chemin possible
        if (posibility == 0) {
            return;
        }
        else {
            int ind = alea(posibility);

            switch (ind) {

            case 0:
                if ((direction.nord != -1) && !((path.begin() + direction.nord)->alreadyPassed)) {

                    (path.begin() + direction.nord)->alreadyPassed = true;

                    // D�finition du point al�atoire en X et en Y
                    (path.begin() + direction.nord)->checkpointX = alea(largeurSousMatrice - 4) + 2;
                    (path.begin() + direction.nord)->checkpointY = alea(hauteurSousMatrice - 4) + 2;

                    //Position de case pr�ceddente et suivante 
                    (path.begin() + direction.nord)->positionBefore = 'S';
                    (path.begin() + start)->positionAfter = 'N';

                    // Liasions entre les cases pr�cedente et suivante
                    (path.begin() + direction.nord)->before = &*(path.begin() + start);
                    (path.begin() + start)->after = &*(path.begin() + direction.nord);

                    goStart = direction.nord;
                    ++totalRoom;
                }
                break;

            case 1:
                if ((direction.sud != -1) && !((path.begin() + direction.sud)->alreadyPassed)) {

                    (path.begin() + direction.sud)->alreadyPassed = true;

                    // D�finition du point al�atoire en X et en Y
                    (path.begin() + direction.sud)->checkpointX = alea(largeurSousMatrice - 4) + 2;
                    (path.begin() + direction.sud)->checkpointY = alea(hauteurSousMatrice - 4) + 2;

                    //Position de case pr�ceddente et suivante 
                    (path.begin() + direction.sud)->positionBefore = 'N';
                    (path.begin() + start)->positionAfter = 'S';

                    // Liasions entre les cases pr�cedente et suivante
                    (path.begin() + direction.sud)->before = &*(path.begin() + start);
                    (path.begin() + start)->after = &*(path.begin() + direction.sud);

                    goStart = direction.sud;
                    ++totalRoom;
                }
                break;

            case 2:
                if ((direction.est != -1) && !((path.begin() + direction.est)->alreadyPassed)) {

                    (path.begin() + direction.est)->alreadyPassed = true;

                    // D�finition du point al�atoire en X et en Y 
                    (path.begin() + direction.est)->checkpointX = alea(largeurSousMatrice - 4) + 2;
                    (path.begin() + direction.est)->checkpointY = alea(hauteurSousMatrice - 4) + 2;

                    //Position de case pr�ceddente et suivante 
                    (path.begin() + direction.est)->positionBefore = 'E';
                    (path.begin() + start)->positionAfter = 'O';

                    // Liasions entre les cases pr�cedente et suivante
                    (path.begin() + direction.est)->before = &*(path.begin() + start);
                    (path.begin() + start)->after = &*(path.begin() + direction.est);

                    goStart = direction.est;
                    ++totalRoom;
                }
                break;

            case 3:
                if ((direction.ouest != -1) && !((path.begin() + direction.ouest)->alreadyPassed)) {

                    (path.begin() + direction.ouest)->alreadyPassed = true;

                    // D�finition du point al�atoire en X et en Y
                    (path.begin() + direction.ouest)->checkpointX = alea(largeurSousMatrice - 4) + 2;
                    (path.begin() + direction.ouest)->checkpointY = alea(hauteurSousMatrice - 4) + 2;

                    //Position de case pr�ceddente et suivante 
                    (path.begin() + direction.ouest)->positionBefore = 'O';
                    (path.begin() + start)->positionAfter = 'E';

                    // Liasions entre les cases pr�cedente et suivante
                    (path.begin() + direction.ouest)->before = &*(path.begin() + start);
                    (path.begin() + start)->after = &*(path.begin() + direction.ouest);

                    goStart = direction.ouest;
                    ++totalRoom;
                }
                break;
            }

            //Initialisation de tous les murs
            size_t res = (largeurSousMatrice) * (hauteurSousMatrice);
            auto roomTarget = path.begin() + goStart;
            for (size_t i = 0; i < res;) {
                if ((int)i < largeurSousMatrice) {
                    roomTarget->matrice[i] = 'W';
                    ++i;
                }
                else if (i < res - largeurSousMatrice) {
                    roomTarget->matrice[i] = 'W';
                    roomTarget->matrice[i + largeurSousMatrice - 1] = 'W';
                    i += largeurSousMatrice;
                }
                else {
                    roomTarget->matrice[i] = 'W';
                    ++i;
                }
            }
            end_room = goStart;
        }


        if (--numberRoom >= 0) {
            build(path, goStart);
        }
    }

    void Procedural::secondBuild(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {

            int ind;
            switch (i->positionAfter) {

            case 'N':
                ind = i->checkpointX + i->checkpointY * largeurSousMatrice;
                while (ind >= 0) {
                    i->matrice[ind] = 'H';
                    ind = ind - largeurSousMatrice;
                }
                break;

            case 'S':
                ind = i->checkpointX + i->checkpointY * largeurSousMatrice;
                while (ind < (hauteurSousMatrice * largeurSousMatrice)) {
                    i->matrice[ind] = 'H';
                    ind = ind + largeurSousMatrice;
                }
                break;

            case 'O':
                ind = i->checkpointX + i->checkpointY * largeurSousMatrice;
                while (ind % largeurSousMatrice != 0) {
                    --ind;
                    i->matrice[ind] = 'S';
                }
                break;

            case 'E':
                ind = i->checkpointX + i->checkpointY * largeurSousMatrice;
                while (((ind % largeurSousMatrice) != (largeurSousMatrice - 1)) && (ind >= 0)) {
                    ++ind;
                    i->matrice[ind] = 'S';
                }
                break;
            }
        }
    }

    // Y pour nord sud 
    void Procedural::thirtyBuild(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {

            int ind, indX, indY;

            switch (i->positionBefore) {

            case 'N':
                ind = 0;
                indX = i->before->checkpointX;
                while (i->checkpointY >= ind) {
                    i->matrice[indX + ind * largeurSousMatrice] = 'H';
                    ++ind;
                }

                break;

            case 'S':
                ind = hauteurSousMatrice - 1;
                indX = i->before->checkpointX;
                while (i->checkpointY <= ind) {
                    i->matrice[indX + ind * largeurSousMatrice] = 'H';
                    --ind;
                }

                break;

            case 'E':
                ind = i->checkpointY;
                indY = i->before->checkpointY;
                if (indY <= ind) {
                    while (indY <= ind) {
                        i->matrice[i->checkpointX + ind * largeurSousMatrice] = 'H';
                        --ind;
                    }
                }
                else if (indY > ind) {
                    while (indY >= ind) {
                        i->matrice[i->checkpointX + ind * largeurSousMatrice] = 'H';
                        ++ind;
                    }
                }

                break;

            case 'O':
                ind = i->checkpointY;
                indY = i->before->checkpointY;
                if (indY <= ind) {
                    while (indY <= ind) {
                        i->matrice[i->checkpointX + ind * largeurSousMatrice] = 'H';
                        --ind;
                    }
                }
                else if (indY > ind) {
                    while (indY >= ind) {
                        i->matrice[i->checkpointX + ind * largeurSousMatrice] = 'H';
                        ++ind;
                    }
                }

                break;
            }
        }
    }

    // X pour Est et Ouest 
    void Procedural::fourtyBuild(std::vector<struct room>& path) {


        for (auto i = path.begin(); i != path.end(); ++i) {

            int ind, indX, indY;

            switch (i->positionBefore) {

            case 'O':
                ind = 0;
                indY = i->before->checkpointY;
                while (i->checkpointX > ind) {
                    i->matrice[ind + indY * largeurSousMatrice] = 'S';
                    ++ind;
                }
                break;

            case 'E':
                ind = largeurSousMatrice - 1;
                indY = i->before->checkpointY;
                while (i->checkpointX < ind) {
                    i->matrice[ind + indY * largeurSousMatrice] = 'S';
                    --ind;
                }
                break;

            case 'N':
                ind = i->checkpointX;
                indX = i->before->checkpointX;
                if (indX < ind) {
                    while (indX < ind) {
                        i->matrice[ind + i->checkpointY * largeurSousMatrice] = 'S';
                        --ind;
                    }
                }
                else if (indX > ind) {
                    while (indX > ind) {
                        i->matrice[ind + i->checkpointY * largeurSousMatrice] = 'S';
                        ++ind;
                    }
                }

                break;

            case 'S':
                ind = i->checkpointX;
                indX = i->before->checkpointX;
                if (indX < ind) {
                    while (indX < ind) {
                        i->matrice[ind + i->checkpointY * largeurSousMatrice] = 'S';
                        --ind;
                    }
                }
                else if (indX > ind) {
                    while (indX > ind) {
                        i->matrice[ind + i->checkpointY * largeurSousMatrice] = 'S';
                        ++ind;
                    }
                }
                break;
            }
        }
    }

    void Procedural::recessLadderBuild(std::vector<char>& matrice, int start) {

        int control = 0;
        int res = largeurSousMatrice * hauteurMatrice;
        int tmp = start;

        int controleLargeur = start % largeurSousMatrice;

        while ((tmp < res) && (matrice[tmp] == 'H')) {
            tmp += largeurSousMatrice;
            ++control;
        }
        // si le chemin est suffisamment long
        if (5 <= control) {
            // On se positionne � un endroit 
            int alea_1 = alea(control / 2) + 1;
            int longueurPlateforme = alea((control - alea_1) - 2) + 1;

            // Si l'echelle est plac� sur le cot� droit,
            // on cr�� un renfoncement � droite
            if ((controleLargeur) <= (largeurSousMatrice / 2)) {

                for (int i = 0; i <= longueurPlateforme; ++i) {
                    matrice[i + start + 1 + (alea_1 * largeurSousMatrice)] = 'S';
                    matrice[i + start + 1 + (alea_1 * largeurSousMatrice) + (3 * largeurSousMatrice)] = 'S';
                }
                for (int j = 0; j < 3; ++j) {
                    matrice[start + (alea_1 * largeurSousMatrice) + (j * largeurSousMatrice) + (longueurPlateforme + 1)] = 'A';
                    if (j != 0) { matrice[start + (alea_1 * largeurSousMatrice) + (j * largeurSousMatrice)] = 'X'; }
                }
            }// sinon on cr�� un renfoncement � gauche
            else {
                for (int i = 0; i < longueurPlateforme; ++i) {
                    matrice[start - 1 - i + (alea_1 * largeurSousMatrice)] = 'S';
                    matrice[start - 1 - i + (alea_1 * largeurSousMatrice) + (3 * largeurSousMatrice)] = 'S';
                }
                // Ascenceur
                for (int j = 0; j < 3; ++j) {
                    matrice[start + 1 + (alea_1 * largeurSousMatrice) + (j * largeurSousMatrice) - (longueurPlateforme + 1)] = 'A';
                    if (j != 0) { matrice[start + (alea_1 * largeurSousMatrice) + (j * largeurSousMatrice)] = 'X'; }
                }
            }
        }
    }

    // S
    void Procedural::recessGroundBuild(std::vector<char>& matrice, int start) {

        size_t control = 0;
        int res = largeurSousMatrice * hauteurSousMatrice;
        int tmp = start;

        int controleLargeur = start / largeurSousMatrice;

        while ((tmp < res) && (matrice[tmp] == 'S')) {
            ++tmp;
            ++control;
        }

        // si le chemin est suffisamment long
        if (5 <= control) {

            // On se positionne � un endroit 
            int alea_1 = alea(control / 2) + 1;
            int longueurPlateforme = alea((control - alea_1) - 2) + 1;;
            // Si l'echelle est plac� sur le cot� droit,
            // on cr�� un renfoncement en bas
            if ((controleLargeur) <= (hauteurSousMatrice / 2)) {

                for (int i = 0; i < 3; ++i) {
                    matrice[start + alea_1 + (i * largeurSousMatrice)] = 'A';
                    matrice[start + alea_1 + (i * largeurSousMatrice) + longueurPlateforme] = 'A';
                }
                for (int j = 1; j < longueurPlateforme; ++j) {
                    matrice[start + alea_1 + (2 * largeurSousMatrice) + j] = 'S';
                    matrice[start + alea_1 + j] = 'X';
                }

            }// sinon on cr�� un renfoncement en haut
            else {
                for (int i = 0; i < 3; ++i) {
                    matrice[start + alea_1 - (i * largeurSousMatrice)] = 'A';
                    matrice[start + alea_1 - (i * largeurSousMatrice) + longueurPlateforme] = 'A';
                }
                for (int j = 1; j < longueurPlateforme; ++j) {
                    matrice[start + alea_1 - (2 * largeurSousMatrice) + j] = 'S';
                    matrice[start + alea_1 + j] = 'X';
                    //matrice[start+(alea_1*largeurSousMatrice)-(j*largeurSousMatrice)]='X';
                }
            }
        }
    }

    void Procedural::recessBuild(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {
            int res = largeurSousMatrice * hauteurSousMatrice;
            if (i->checkpointX != -1) {
                for (int j = 0; j < res; ++j) {
                    if (i->matrice[j] == 'H') {
                        recessLadderBuild(i->matrice, j);
                        break;
                    }
                    else if (i->matrice[j] == 'S') {
                        recessGroundBuild(i->matrice, j);
                        break;
                    }
                }
            }
        }
    }


    void Procedural::mobilePlatformBuild(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {
            int res = largeurSousMatrice * hauteurSousMatrice;
            bool ok = false;
            if (i->checkpointX != -1) {
                for (int j = 0; j < res; ++j) {
                    if (i->matrice[j] == 'S') {
                        ok = true;
                        for (int k = 0; k < 5; ++k) {
                            if ((j + k) % largeurSousMatrice == largeurSousMatrice - 1) { ok = false; break; }
                            // Il ne faut rien au dessus
                            if ((i->matrice[j + k] != 'S') && (i->matrice[j - largeurSousMatrice] != ' ')) {
                                ok = false;
                                break;
                            }
                        }
                        if (ok) {
                            int alea_1 = alea(5);
                            // Plateforme mobile
                            if (alea_1 == 0) {
                                for (int l = 0; l < 3; ++l) {
                                    i->matrice[j + l + 1] = 'M';
                                }
                            }// Plateforme qui tombe 
                            else if (alea_1 == 1) {
                                i->matrice[j + 3] = 'T';
                            }
                            j = j + 10;
                        }
                    }
                }
            }
        }
    }

    bool Procedural::controlPlacementEnnemy(std::vector<char>& matrice, int start, int mobile) {

        // S'il y a d�passement sur le cot� de la sous-matrice
        if (((start % largeurSousMatrice) + mobile) >= largeurSousMatrice) {
            return false;
        }

        // S'il y a d�passement par le bas de la matrice 
        if ((start + largeurSousMatrice * 3) >= largeurSousMatrice * hauteurMatrice) {
            return false;
        }

        for (int i = 0; i < mobile; ++i) {

            if ((matrice[start + i] != ' ') || (matrice[start + i + largeurSousMatrice] != ' ') || (matrice[start + i + (largeurSousMatrice * 2)] != ' ')) {
                return false;
            }
        }
        if ((matrice[start + largeurSousMatrice * 2 - 1] != ' ') || (matrice[start + largeurSousMatrice * 2 + mobile] != ' ')) {
            return false;
        }
        return true;
    }

    void Procedural::introduceEnnemy(std::vector<char>& matrice, int start) {

        for (size_t i = 0; i < 5; ++i) {
            matrice[start + i] = 'X';
            if (i % 4 == 0) {
                matrice[start + i + largeurSousMatrice] = 'X';
            }
            else {
                matrice[start + i + largeurSousMatrice] = 'E';
            }
            matrice[start + i + largeurSousMatrice * 2] = 'S';
        }
        matrice[start + largeurSousMatrice * 2 - 1] = 'S';
        matrice[start + largeurSousMatrice * 2 + 5] = 'S';
    }
    void Procedural::introduceTrap(std::vector<char>& matrice, int start) {

        for (size_t i = 0; i < 3; ++i) {
            matrice[start + i] = 'X';
            if (i % 2 == 0) {
                matrice[start + i + largeurSousMatrice] = 'X';
            }
            else {
                matrice[start + i + largeurSousMatrice] = 'C';
            }
            matrice[start + i + largeurSousMatrice * 2] = 'S';
        }
        matrice[start + largeurSousMatrice * 2 - 1] = 'S';
        matrice[start + largeurSousMatrice * 2 + 3] = 'S';
    }

    void Procedural::createEnnemyTrapMortal(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {
            if (i->checkpointX == -1) { continue; }
            // Placement des ennemis
            for (int j = 0; j < 20; ++j) {
                int tmp = alea(hauteurSousMatrice * largeurSousMatrice);
                if (controlPlacementEnnemy(i->matrice, tmp, 5)) {
                    introduceEnnemy(i->matrice, tmp);
                }
            }
            // Placement des pieges mortel
            for (int j = 0; j < 20; ++j) {
                int tmp = alea(hauteurSousMatrice * largeurSousMatrice);
                if (controlPlacementEnnemy(i->matrice, tmp, 3)) {
                    introduceTrap(i->matrice, tmp);
                }
            }
        }
    }

    bool Procedural::completeLiana(std::vector<char>& matrice, int start) {

        int below_index = largeurSousMatrice;
        int res = largeurSousMatrice * hauteurSousMatrice;
        if (matrice[start] == ' ') {
            matrice[start] = 'H';
        }
        while ((start + below_index) < res) {
            if (matrice[start + below_index] != ' ') {
                break;
            }
            matrice[start + below_index] = 'H';
            below_index += largeurSousMatrice;
        }
        if (res <= (start + below_index)) {
            return false;
        }
        return true;
    }

    void Procedural::createLiana(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {
            if (i->checkpointX == -1) { continue; }
            int res = largeurSousMatrice * hauteurSousMatrice;

            for (int j = 0; j < res; ++j) {
                // Si une liane doit �tre construite 
                if (i->matrice[j] == 'H') {
                    if ((j + largeurSousMatrice) < res) {
                        if (i->matrice[j + largeurSousMatrice] != 'H') {
                            bool retur = completeLiana(i->matrice, j);
                            while (!retur) {
                                retur = completeLiana((i + largeurMatrice)->matrice, j % largeurSousMatrice);
                            }
                        }
                    }


                }
            }
        }
    }

    void Procedural::hiddenPath(std::vector<struct room>& path) {

        size_t limite = largeurSousMatrice + 1;
        size_t maxLength = 6;
        for (auto h = path.begin(); h != path.end(); ++h) {

            if (h->checkpointX != -1) {
                // Pour mettre qu'une liane par plateforme 
                bool liana_present = false;

                // pour chaque tuile 
                for (size_t i = (size_t)largeurSousMatrice + 1; i < largeurSousMatrice * (hauteurSousMatrice - 1) - maxLength - 3; ++i) {
                    ++limite;
                    // Empeche la modification du 'bon' chemin 
                    // Empeche l'implantation de plateforme sur les murs de la sous matrice 
                    if ((h->matrice[i] != ' ') || (limite % largeurSousMatrice == 0) || (limite % largeurSousMatrice == (size_t)(largeurSousMatrice - 1))) { continue; }

                    int alea_1 = alea(4); // 1 chance sur 2

                    // Si la condition est correct, alors on g�n�re une plateforme 
                    if (alea_1 == 0) {
                        // La longueur de la plateforme (minimum 1 - max 4)
                        int alea_2 = alea(maxLength) + 3;
                        for (int l = i; (size_t)l < (i + alea_2); ++l) {
                            // on controle qu'il n'y a pas de plateforme juste en dessous 
                            // afin de ne pas d�truire le chemin sinon on stop le traitement 
                            // et on continue 
                            if (
                                // au dessus 
                                (h->matrice[l - largeurSousMatrice] == ' ')
                                &&
                                // en dessous 
                                (h->matrice[l + largeurSousMatrice] == ' ')
                                &&
                                // A gauche 
                                //(h->matrice[l-1] == ' ')
                                //&&
                                // A droite 
                                (h->matrice[l + 1] == ' ')
                                ) {

                                // Echelle 
                                int alea_3 = alea(20);
                                if ((alea_3 % 19 == 0) && (!liana_present)) {
                                    h->matrice[l] = 'H';
                                    liana_present = true;
                                }
                                else {
                                    h->matrice[l] = 'S';
                                }
                            }
                            else {
                                continue;
                            }

                            liana_present = false;
                        }
                        i = i + alea_2 + 1;
                    }
                }
            }
        }
    }

    void Procedural::buildWall(std::vector<struct room>& path) {

        for (auto i = path.begin(); i != path.end(); ++i) {
            size_t res;
            switch (i->positionBefore) {
            case 'N':
                for (int nb = 1; nb < largeurSousMatrice - 1; ++nb) {
                    i->matrice[nb] = ' ';
                }
                break;
            case 'S':
                res = largeurSousMatrice * (hauteurSousMatrice - 1);
                for (int sb = 1; sb < largeurSousMatrice - 1; ++sb) {
                    i->matrice[sb + res] = ' ';
                }
                break;
            case 'E':
                for (int eb = 2; eb < hauteurSousMatrice; ++eb) {
                    i->matrice[eb * largeurSousMatrice - 1] = ' ';
                }
                break;
            case 'O':
                for (int ob = 1; ob < hauteurSousMatrice - 1; ++ob) {
                    i->matrice[ob * largeurSousMatrice] = ' ';
                }
                break;
            }

            switch (i->positionAfter) {
            case 'N':
                for (int na = 1; na < largeurSousMatrice - 1; ++na) {
                    i->matrice[na] = ' ';
                }
                break;
            case 'S':
                res = largeurSousMatrice * (hauteurSousMatrice - 1);
                for (int sa = 1; sa < largeurSousMatrice - 1; ++sa) {
                    i->matrice[sa + res] = ' ';
                }
                break;
            case 'E':
                for (int ea = 2; ea < hauteurSousMatrice; ++ea) {
                    i->matrice[ea * largeurSousMatrice - 1] = ' ';
                }
                break;
            case 'O':
                for (int oa = 1; oa < hauteurSousMatrice - 1; ++oa) {
                    i->matrice[oa * largeurSousMatrice] = ' ';
                }
                break;
            }
        }
    }

    void Procedural::imp(std::vector<struct room>& path) {

        //(map.begin() + end_room)->matrice[(map.begin() + end_room)->checkpointX + ((map.begin() + end_room)->checkpointY - 1) * largeurSousMatrice] = 'F';

        std::ofstream monFlux2("./res2.txt");
        int m = 0;
        int indice = 0;
        bool ok = false;
        for (int q = 0; q < (largeurSousMatrice * 7); ++q) {
            if (ok) { monFlux2 << "-"; }
        }
        if (ok) { monFlux2 << "-"; }

        // parcourir les cases de la matrice en hauteurMatrice 
        for (int x = 0; x < hauteurMatrice; ++x) {

            if (ok) { monFlux2 << '\n'; }

            for (int w = 0; w < hauteurSousMatrice; ++w) {
                for (int y = 0; y < largeurMatrice; ++y) {
                    if (ok) { monFlux2 << "|"; }
                    for (int z = 0; z < largeurSousMatrice; ++z) {
                        ++m;
                        monFlux2 << ((path.begin() + (x * largeurMatrice) + (y))->matrice[z + (w * largeurSousMatrice)]);
                    }
                }

                //monFlux2 << "|";
                monFlux2 << '\n';
            }

            //monFlux << "\n";
            for (int q = 0; q < (largeurSousMatrice * 7); ++q) {
                if (ok) { monFlux2 << "-"; }
            }
            if (ok) { monFlux2 << "-"; }
            ++indice;
        }
    }

    void Procedural::clearMap(std::vector<struct room>& path) {

        int startX = (path.begin() + start_room)->checkpointX;
        int startY = (path.begin() + start_room)->checkpointY;
        (path.begin() + start_room)->matrice[startX + startY * largeurSousMatrice] = 'S';
        (path.begin() + start_room)->matrice[startX + (startY - 1) * largeurSousMatrice] = ' ';

        int endX = (path.begin() + end_room)->checkpointX;
        int endY = (path.begin() + end_room)->checkpointY;
        (path.begin() + start_room)->matrice[endX + endY * largeurSousMatrice] = 'S';
        (path.begin() + start_room)->matrice[endX + (endY - 1) * largeurSousMatrice] = ' ';


        for (auto i = path.begin(); i != path.end(); ++i) {
            if (i->checkpointX != -1) {
                int res = largeurSousMatrice * hauteurSousMatrice;
                for (int j = 0; j < res; ++j) {
                    if (i->matrice[j] == 'M') {
                        i->matrice[j + 1] = ' ';
                        i->matrice[j + 2] = ' ';
                    }
                    else if (i->matrice[j] == 'E') {
                        i->matrice[j + 1] = ' ';
                        i->matrice[j + 2] = ' ';
                    }
                    else if (i->matrice[j] == 'A') {
                        i->matrice[j + largeurSousMatrice] = ' ';
                        i->matrice[j + 2 * largeurSousMatrice] = ' ';
                    }
                }
            }
        }
    }
    void Procedural::startPlayer(struct room start) {

        int x = start.checkpointX;
        int y = start.checkpointY;
        int indice = x + y * largeurSousMatrice;

        if ((start.matrice[indice - largeurSousMatrice] == ' ') && (start.matrice[indice] == 'S')) {
            return;
        }
        // A gauche 
        else if ((start.matrice[indice - largeurSousMatrice - 1] == ' ') && (start.matrice[indice - 1] == 'S')) {
            start.checkpointX -= 1;
        } //A droite 
        else if ((start.matrice[indice - largeurSousMatrice - 1] == ' ') && (start.matrice[indice - 1] == 'S')) {
            start.checkpointX -= 1;
        }

    }


    void Procedural::primary() {

        srand(time(NULL));

        start_room = alea(hauteurMatrice * largeurMatrice);

        for (int i = 0; i < largeurMatrice * hauteurMatrice; ++i) {
            struct room first_room;

            first_room.alreadyPassed = false;

            first_room.positionBefore = '\0';
            first_room.positionAfter = '\0';

            first_room.checkpointX = -1;
            first_room.checkpointY = -1;

            first_room.before = NULL;
            first_room.after = NULL;

            for (int k = 0; k < (hauteurSousMatrice * largeurSousMatrice); ++k) {
                first_room.matrice.push_back(' ');
            }

            map.push_back(first_room);
        }

        auto first = map.begin() + start_room;

        first->checkpointX = alea(largeurSousMatrice - 4) + 2;
        first->checkpointY = alea(hauteurSousMatrice - 4) + 2;

        first->alreadyPassed = true;

        int res = (largeurSousMatrice) * (hauteurSousMatrice);

        for (int i = 0; i < res;) {
            if (i < largeurSousMatrice) {
                first->matrice[i] = 'W';
                ++i;
            }
            else if (i < res - largeurSousMatrice) {
                first->matrice[i] = 'W';
                first->matrice[i + largeurSousMatrice - 1] = 'W';
                i += largeurSousMatrice;
            }
            else {
                first->matrice[i] = 'W';
                ++i;
            }
        }

        build(map, start_room);
        buildWall(map);

        secondBuild(map);
        thirtyBuild(map);
        fourtyBuild(map);
        recessBuild(map);

        createEnnemyTrapMortal(map);

        hiddenPath(map);
        createLiana(map);
        mobilePlatformBuild(map);

        clearMap(map);

        startPlayer(*(map.begin() + start_room));

        //joueur
        //(map.begin() + start_room)->matrice[(map.begin() + start_room)->checkpointX + ((map.begin() + start_room)->checkpointY - 1) * largeurSousMatrice] = 'P';
        playerPosition(map);

        //point de fin
        (map.begin() + end_room)->matrice[(map.begin() + end_room)->checkpointX + ((map.begin() + end_room)->checkpointY - 1) * largeurSousMatrice] = 'F';

        imp(map);

    }
}