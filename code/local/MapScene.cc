/*

    MAXEL Killian TP1A
    DEMOUGIN Alexandre TP1A
    RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>
#include <gf/RenderTarget.h>
#include <gf/ResourceManager.h>
#include <gf/MessageManager.h>
#include <gf/Random.h>
#include <gf/Coordinates.h>
#include <gf/Log.h>

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "config.h"

#include <gf/RenderTarget.h>
#include <gf/Singleton.h>
#include <gf/Unused.h>
#include <gf/Message.h>
#include <gf/Collision.h>
#include <gf/TileLayer.h>

#include "MapScene.h"
#include "Singletons.h"
#include "Messages.h"

#include "box2d/box2d.h"

#include "Player.h"

#include <gf/Log.h>
#include <gf/RenderTarget.h>
#include <gf/Shapes.h>
#include <gf/Tmx.h>
#include <gf/TmxOps.h>
#include <gf/SpriteBatch.h>

namespace plateformer2021 {


    static gf::Texture* loadDisplayTexture(const gf::Path& path) {
        gf::Texture& texture = plateformer2021::rsc().getTexture(path);
        texture.setSmooth();
        return &texture;
    }

    MapScene::MapScene(Player p)
        : gf::Entity(100)
        , playerCoords(p.position.x, p.position.y)
        , isOnEndingPoint(false)
        , finished(false)
        , playerP(&p)
        , numberOfSprites(12)
        , endFlagPos({ 0.0f,0.0f })
        , player(p)
        ,playerBody(NULL)
        ,worldP(NULL)
    {
        plateformer2021::msgMnger().registerHandler<NextMapMessage>(&MapScene::onNewMap, this);//niveau franchi
        plateformer2021::msgMnger().registerHandler<ResetMapMessage>(&MapScene::onResetMap, this);//r�initialisation map
        plateformer2021::msgMnger().registerHandler<LostMessage>(&MapScene::onMapLost, this);//l'�chec d'un niveau (mort ?)
        plateformer2021::msgMnger().registerHandler<OnPlayerPositionMessage>(&MapScene::onPlayerPosition, this);//objet/bloc transparent � la position du joueur
        plateformer2021::msgMnger().registerHandler<OnEndingPointMessage>(&MapScene::onEndingPoint, this);//le joueur est sur le point de fin
        msgMnger().registerHandler<OnPlayerPositionMessage>(&MapScene::onPlayerPosition, this);
    }


    //tuto : 485,223
    void MapScene::drawEndFlag(gf::RenderTarget& target, const gf::RenderStates& states) {
        gf::Texture& endFlagTexture = rsc().getTexture("texture/signal_flag.png");
        endFlagTexture.setSmooth();
        gf::Sprite endFlagSprite;
        endFlagSprite.setPosition({ endFlagPos.x * 100 - 12.0f, endFlagPos.y * 100 - 12.0f });
        endFlagSprite.setTexture(endFlagTexture);
        endFlagSprite.setScale({ 0.5 });
        target.draw(endFlagSprite, states);
    }

    void MapScene::update(gf::Time time) {
        /*printf("(%f,%f)\n", playerCoords.x, playerCoords.y);
        if (playerCoords.x >= 485 && playerCoords.y >= 220) {
            printf("VICTOIRE\n");
        }*/

    }

    void MapScene::drawMap(gf::RenderTarget& target, const gf::RenderStates& states){
        /*gf::Texture& backgroundTexture = rsc().getTexture("texture/background.png");
        backgroundTexture.setSmooth();
        gf::Sprite backgroundSprite;
        backgroundSprite.setPosition({ -200,0 });
        backgroundSprite.setTexture(backgroundTexture);
        backgroundSprite.setScale({ 1.55,0.85 });
        target.draw(backgroundSprite, states);*/

        gf::Texture& groundTexture = rsc().getTexture("texture/ground1.png");
        gf::Sprite groundSprite;
        groundSprite.setTexture(groundTexture);
        groundSprite.setScale(0.5);

        gf::SpriteBatch staticBatch(target);
        staticBatch.begin();

        for (size_t i = 0; i < staticBodiesFromPhysics.size(); i++) {
            float x = staticBodiesFromPhysics.at(i)->GetPosition().x;
            float y = staticBodiesFromPhysics.at(i)->GetPosition().y;
            groundSprite.setPosition({ x*100-12.0f, y*100 - 13.0f });
            staticBatch.draw(groundSprite, states);
        }
        staticBatch.end();

        //murs

        gf::Texture& wallTexture = rsc().getTexture("texture/wall.png");
        gf::Sprite wallSprite;
        wallSprite.setTexture(wallTexture);
        wallSprite.setScale(0.5);

        gf::SpriteBatch wallBatch(target);
        wallBatch.begin();

        for (size_t i = 0; i < wallBodiesFromPhysics.size(); i++) {
            float x = wallBodiesFromPhysics.at(i)->GetPosition().x;
            float y = wallBodiesFromPhysics.at(i)->GetPosition().y;
            wallSprite.setPosition({ x * 100 - 12.0f, y * 100 - 13.0f });
            wallBatch.draw(wallSprite, states);
        }
        wallBatch.end();

        //blocs poussables
        for (size_t i = 0; i < pushableBodiesFromPhysics.size(); i++) {
            float x = pushableBodiesFromPhysics.at(i)->GetPosition().x;
            float y = pushableBodiesFromPhysics.at(i)->GetPosition().y;
            groundSprite.setPosition({ x *100-12.0f, y*100 - 12.0f });
            target.draw(groundSprite, states);
        }

        for (size_t i = 0; i < movingBodiesFromPhysics.size(); i++) {
            float x = movingBodiesFromPhysics.at(i)->GetPosition().x;
            float y = movingBodiesFromPhysics.at(i)->GetPosition().y;
            groundSprite.setPosition({ x*100-12.0f, y*100 - 13.0f });
            target.draw(groundSprite, states);
        }

        for (size_t i = 0; i < elevatorBodiesFromPhysics.size(); i++) {
            float x = elevatorBodiesFromPhysics.at(i)->GetPosition().x;
            float y = elevatorBodiesFromPhysics.at(i)->GetPosition().y;
            groundSprite.setPosition({ x * 100 - 12.0f, y * 100 - 13.0f });
            target.draw(groundSprite, states);
        }

        for (size_t i = 0; i < fallingBodiesFromPhysics.size(); i++) {
            float x = fallingBodiesFromPhysics.at(i)->GetPosition().x;
            float y = fallingBodiesFromPhysics.at(i)->GetPosition().y;
            groundSprite.setPosition({ x * 100 - 12.0f, y * 100 - 13.0f });
            if (cling.at(i) && fallingBodiesFromPhysics.at(i)->GetPosition()== fallingBodiesInitialPosFromPhysics.at(i)) {
                groundSprite.setColor(gf::Color::Red);
            }
            else {
                groundSprite.setColor(gf::Color::White);
            }
            target.draw(groundSprite, states);
        }


        gf::Texture& ennemyTexture = rsc().getTexture("texture/ennemy.png");
        gf::Sprite ennemySprite;
        ennemySprite.setTexture(ennemyTexture);
        ennemySprite.setScale(0.25);
        for (size_t i = 0; i < ennemiesBodiesFromPhysics.size(); i++) {
            float x = ennemiesBodiesFromPhysics.at(i)->GetPosition().x;
            float y = ennemiesBodiesFromPhysics.at(i)->GetPosition().y;
            ennemySprite.setPosition({ x * 100 - 5.0f, y * 100 - 6.0f });
            target.draw(ennemySprite, states);
        }

        gf::Texture& ladderTexture = rsc().getTexture("texture/ladder.png");
        gf::Sprite ladderSprite;
        ladderSprite.setTexture(ladderTexture);
        ladderSprite.setScale(0.5);

        gf::SpriteBatch climbedBatch(target);
        climbedBatch.begin();

        for (size_t i = 0; i < climbedBodiesFromPhysics.size(); i++) {
            float x = climbedBodiesFromPhysics.at(i)->GetPosition().x;
            float y = climbedBodiesFromPhysics.at(i)->GetPosition().y;
            ladderSprite.setPosition({ x * 100 - 12.0f, y * 100 - 13.0f });
            climbedBatch.draw(ladderSprite, states);
        }
        climbedBatch.end();

        gf::Texture& trapTexture = rsc().getTexture("texture/trap.png");
        gf::Sprite trapSprite;
        trapSprite.setTexture(trapTexture);
        trapSprite.setScale(0.5);
        trapSprite.setOrigin({ 25.0f,25.0f});
        for (size_t i = 0; i < trapBodiesFromPhysics.size(); i++) {
            float x = trapBodiesFromPhysics.at(i)->GetPosition().x*100.0f;
            float y = trapBodiesFromPhysics.at(i)->GetPosition().y*100.0f;
            float angle = trapBodiesFromPhysics.at(i)->GetAngle();

            float posRotationX = x * cos(angle) - y * sin(angle);
            float posRotationY = x * sin(angle) + y * cos(angle);

            trapSprite.setPosition({ x,y });
            trapSprite.setRotation(angle);
            //trapSprite.setPosition({ posRotationX * 100 - 12.0f, posRotationY * 100 - 13.0f });
            //trapSprite.setRotation(angle);
            target.draw(trapSprite, states);
        }

        if (endFlagPos.x!=0 && endFlagPos.y!=0){
            drawEndFlag(target,states);
        }

        /*gf::Texture& endPointFlagTexture = rsc().getTexture("texture/signal_flag.png");
        endPointFlagTexture.setSmooth();
        gf::Sprite endPointFlagSprite;
        endPointFlagSprite.setTexture(endPointFlagTexture);
        endPointFlagSprite.setScale(0.5);
        endPointFlagSprite.setPosition({ 485,223 });
        target.draw(endPointFlagSprite, states);*/
    }

    void MapScene::render(gf::RenderTarget& target, const gf::RenderStates& states) {
        drawMap(target, states);//dessiner chaque bloc fait laguer
    }

    /*void MapScene::generateWorld(gf::Random& random) {
        gf::Vector2i coords(0,0);
        finished = false;
    }

    void MapScene::clearMap() {
        plateformer2021::LostMessage msg;
        plateformer2021::msgMnger().sendMessage(&msg);
    }*/

    gf::MessageStatus MapScene::onNewMap(gf::Id id, gf::Message* msg) {
        gf::unused(id);
        gf::unused(msg);

        //clearMap();
        return gf::MessageStatus::Keep;
    }

    gf::MessageStatus MapScene::onResetMap(gf::Id id, gf::Message* msg) {
        gf::unused(id);
        gf::unused(msg);
        return gf::MessageStatus::Keep;
    }

    gf::MessageStatus MapScene::onMapLost(gf::Id id, gf::Message* msg) {
        gf::unused(id);
        gf::unused(msg);
        return gf::MessageStatus::Keep;
    }

    gf::MessageStatus MapScene::onPlayerPosition(gf::Id id, gf::Message* msg) {
        /*gf::unused(id);

        auto plPos = static_cast<PlayerPositionMessage*>(msg);
        //g�rer items ou blocs

        return gf::MessageStatus::Keep;*/
        //assert(id == PlayerPositionMessage::type);
        auto hero2 = static_cast<PlayerPositionMessage*>(msg);
        //position = hero2->position;
        gf::unused(id);
        gf::unused(msg);
        return gf::MessageStatus::Keep;

    }

    gf::MessageStatus MapScene::onEndingPoint(gf::Id id, gf::Message* msg) {
        gf::unused(id);
        gf::unused(msg);

        return gf::MessageStatus::Keep;
    }
}