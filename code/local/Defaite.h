#ifndef PLATEFORMER2021_DEFAITE_H
#define PLATEFORMER2021_DEFAITE_H


#include <gf/Entity.h>
#include <gf/Texture.h>

namespace plateformer2021 {

  class Defaite : public gf::Entity {
  public:
      Defaite();

    gf::Vector2f positionMort;

    const gf::Texture& m_texture;
    const gf::Texture& m_textTexture;

    std::string aleaScreenDefaite();
    std::string aleaTextDefaite();

    void setPositionMort(float x, float y);

    virtual void render(gf::RenderTarget& target, const gf::RenderStates& states) override;
  };

}

#endif // PLATEFORMER2021_DEFAITE_H
