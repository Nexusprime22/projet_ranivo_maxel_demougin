/*

	MAXEL Killian TP1A
	DEMOUGIN Alexandre TP1A
	RANIVO Michael TP2A

*/
#include <gf/Action.h>
#include <gf/Clock.h>
#include <gf/Color.h>
#include <gf/EntityContainer.h>
#include <gf/Event.h>
#include <gf/RenderWindow.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/Window.h>
#include <gf/Image.h>
#include <gf/Font.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/Texture.h>

#include "../build/config.h"

#include "local/Menu.h"

#include <iostream>
#include <cstdlib>
#include <cassert>

#include "local/Singletons.h"
#include "local/Messages.h"

namespace plateformer2021{
}

int main() {

	static constexpr gf::Vector2u ScreenSize(1024, 576);
	static constexpr gf::Vector2i ViewSize(200.0f, 200.0f);
	static constexpr gf::Vector2i ViewCenter(0.0f, 0.0f);

	gf::Window window("Plateformer2021", ScreenSize);
	plateformer2021::Menu mainMenu;
	mainMenu.initMenu(window,1,1);
	return 0;

}
